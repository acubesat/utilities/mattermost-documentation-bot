// The array of entries and some data they hold
let entries = [];

const fuseOptions = {
    // id: "index",
    shouldSort: false,
    includeScore: true,
    includeMatches: true,
    threshold: 0.3,
    location: 0,
    distance: 1000,
    maxPatternLength: 32,
    minMatchCharLength: 2,
    keys: [
        "docid",
        "title",
        "author"
    ]
};

let fuse;
let applier;


window.onload = function() {
        _.each(document.getElementsByClassName('js--entry'), function(el) {
        // Create a new entry
        try {
            let entry = JSON.parse(el.getAttribute('data-data'));
            entry.element = el; // Add the element as well so it can be manipulated later
            entry.id = el.id;
            entry.index = entries.length; // Add the element index, useful for fuse

            entries.push(entry);
        } catch (e) {
            console.exception("Could not parse " + el.id);
        }
    });

    fuse = new Fuse(entries, fuseOptions); // "list" is the item array
    rangy.init();
    applier = rangy.createClassApplier("uk-text-primary");

    // Focus the element in case focus was lost by some of the above code
    document.getElementById('js--input-search').focus();

    let rangesApplied = [];
    let exactSearch = function(query) {
        query = query.toLowerCase(); // convert query to lowercase
        let results = [];


        _.each(entries, function(entry) {
            let matches = [];
            _.each(fuseOptions.keys, function(key) {
                const haystack = entry[key].toLowerCase(); // the string inside which to search

                let position = -1;
                while(true) {
                    position = haystack.indexOf(query, position + 1);

                    if (position !== -1) {
                        matches.push({
                            key: key,
                            indices: [[position, position + query.length]]
                        })
                    } else {
                        break;
                    }
                }
            });

            if (!_.isEmpty(matches)) {
                results.push({
                    item: entry,
                    matches: matches
                })
            }
        });

        return results;
    };
    let performSearch = _.throttle(function(query) { // A throttled function that performs the search (to prevent lag)
        console.log("Performing search on " + query);

        _.each(rangesApplied, function(range) {
            // Remove all formatting
            applier.undoToRange(range);
        });
        rangesApplied = [];

        if (query.length <= 1) {
            // For small queries, just show all entries
            _.each(entries, function(val) {
                val.element.style.display = 'table-row';
            })
        } else {
            // First, perform an exact-text search
            let results = exactSearch(query);
            if (_.isEmpty(results)) {
                // If no results are found, take the extra time to perform a fuzzy search
                results = fuse.search(query);
            }

            _.each(entries, function(entry) {
                entry.element.style.display = 'none'; // Clear styling from all entries
            });

            let counter = 0;
            _.each(results, function(result) {
                result.item.element.style.display = 'table-row'; // These entries are visible

                if (counter++ < 20) { // Do not highlight too many results, to prevent lag
                    // Now highlight the interesting parts
                    _.each(result.matches, function (match) {

                        let child = result.item.element.querySelector(".js--entry-" + match.key); // Find what was highlighted
                        _.each(match.indices, function (index) {
                            const range = rangy.createRange();
                            range.selectCharacters(child, index[0], index[1] + 1);
                            applier.applyToRange(range);
                            rangesApplied.push(range); // Store to list so that we can clear it later
                        });
                    });
                }
            });
        }
    }, 700);

    // Perform the search on input
    document.getElementById('js--input-search').oninput = function() {
        performSearch(this.value);
    };

    // Whenever a user presses a special key on the search input, we assume that they wanted to scroll the page instead
    document.getElementById('js--input-search').onkeydown = function (e) {
        const code = e.code; // Get the pressed key code
        // Check for some special searches
        if (code.startsWith("Page") || code === "ArrowUp" || code === "ArrowDown" || code === "End" || code === "Home") {
            this.blur(); // Unfocus the search box

            // Create a new keyboard event to be fired to the window
            let keyboardEvent = document.createEvent("KeyboardEvent");
            const initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";

            keyboardEvent[initMethod](
                "keydown", // event type: keydown, keyup, keypress
                true,      // bubbles
                true,      // cancelable
                window,    // view: should be window
                false,     // ctrlKey
                false,     // altKey
                false,     // shiftKey
                false,     // metaKey
                code,      // keyCode: unsigned long - the virtual key code, else 0
                0          // charCode: unsigned long - the Unicode character associated with the depressed key, else 0
            );
            document.dispatchEvent(keyboardEvent);
        }
    };

    _.each(document.getElementsByClassName('js--quick-preview'), function(elem) {
        elem.onclick = function (e) {
            SimpleLightbox.open({
                content: '<div class="db-iframe-container"><iframe src="' + this.getAttribute('href') + '" frameborder="0" style="width:90vw; height:87vh"></iframe></div>',
                elementClass: 'slbContentEl'
            });

            e.preventDefault();
            return false;
        }
    });
};


