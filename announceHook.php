<?php

// Receives a request from Mattermost, and performs the corresponding announce

require_once __DIR__ . '/vendor/autoload.php';

$data = json_decode(file_get_contents('php://input'), true);

$docid = trim($data['context']['docid'], '-'); // Sanity check
if (strlen($docid) > 254 || strlen($docid) < 5) die(); // Sanity check

if ($_SERVER['REQUEST_METHOD'] !== 'POST') die(); // Only POST requests

system('php ' . __DIR__ . '/console.php announce ' . escapeshellarg($docid) . ' &');
