<?php

use App\Chat\MattermostAPI;
use App\Operations\Kernel;
use App\Operations\LDAP;
use App\Parser\PDFParser;
use Psr\Log\LoggerInterface;

header("Content-Type: application/json");

// Initialise stuff
require __DIR__ . '/vendor/autoload.php';
$container = Kernel::getWebContainer();

$options = [];

class Option {
    public $children; /** The children Options of this Option */
    public $id; /** The unique integer identifier of this Option */
    public $question; /** The text message to display for this attachment when called to choose the children of this Option */
    public $name; /** The display name of this option */
    public $value; /** How this option will be displayed in the final string */
    public $parent; /** The parent Part or Option of this Option */
    public $part; /** The parent Part of this Option */

    public function __construct(string $value, string $name, string $question = "", array $children = []) {
        global $options;

        $this->value = $value;
        $this->name = $name;
        $this->children = $children;
        $this->question = $question;
        $this->id = count($options);
        $options[] = $this;

        foreach ($this->children as $child) {
            $child->parent = $this;
        }
    }

    public function setPart($part) {
        $this->part = $part;
        foreach ($this->children as $child) {
            $child->setPart($part);
        }
    }
}

class Part {
    public $question;
    public $children;

    public $prefix = "-"; /** A string to include before the part starts */
    public $postfix = ""; /** A string to include after the part ends */

    function __construct(string $question = "", array $options = []) {
        $this->children = $options;
        $this->question = $question;

        foreach ($this->children as $child) {
            $child->setPart($this);
        }
    }

    public function prefix($prefix) {
        $this->prefix = $prefix;
        return $this;
    }
    public function postfix($postfix) {
        $this->postfix = $postfix;
        return $this;
    }
}

$parts = [
    (new Part("First, choose the type of document you are about to write:", [
        new Option("GEN", "General"),
        new Option("ADC", "Attitude Determination & Control"),
        new Option("COM", "Communications"),
        new Option("EPS", "Electrical Power"),
        new Option("OBC", "On-Board Data Handling"),
        new Option("PRG", "Programmatic"),
        new Option("SCI", "Science Unit"),
        new Option("STR", "Structural"),
        new Option("THE", "Thermal"),
        new Option("SYE", "Systems Engineering"),
        new Option("TRA", "Trajectory"),
        new Option("COL", "Collaboration"),
    ]))->prefix("AcubeSAT-"),
    new Part("Choose the **type** of your document:", [
        new Option("E", "Experimentation & Development", "What kind of item are you developing?", [
            new Option("C", "Components"),
            new Option("S", "Standards"),
            new Option("T", "Technologies"),
            new Option("W", "Software"),
            new Option("H", "Theoretical")
        ]),
        new Option("B", "Research & Technical Background", "What kind of item are you researching?", [
            new Option("C", "Components"),
            new Option("S", "Standards"),
            new Option("T", "Technologies"),
            new Option("W", "Software"),
            new Option("H", "Theoretical")
        ]),
        new Option("M", "Meeting Outcomes", "- **Internal**: With CubeSAT members only.\n- **External**: Meetings with other experts", [
            new Option("I", "Internal"),
            new Option("X", "External"),
        ]),
        new Option("T", "Technical Specification", "What kind of specification is this?", [
            new Option("G", "Generic"),
            new Option("U", "Subsystem specific"),
        ]),
        new Option("G", "Technical Guides & Handbooks"),
        new Option("O", "Generic & Operational", "Choose the type of document:", [
            new Option("L", "Letters"),
            new Option("P", "Planning"),
            new Option("R", "Presentations"),
            new Option("A", "Policies & Procedures"),
        ])
    ])
];

$response = [
    "response_type" => "in_channel",
    "username" => "Documentation Helper",
    "icon_url" => "https://helit.org/mm_icon_doc_id_large.png"
];
$attachment = [];

// Read request POST JSON data
$data = file_get_contents('php://input');
file_put_contents("a", $data);
$data = json_decode($data, true);
if ($data) {
    $_REQUEST += $data;
    $_REQUEST += $data["context"]; // Due to the operation of +, context
}
file_put_contents("b", json_encode($_REQUEST));

// Some housekeeping for the response
if (isset($_REQUEST['channel_name'])) {
    // Response was sent in Mattermost
    if (strpos($_REQUEST['channel_name'], "__") === false && strpos($_REQUEST['channel_name'], "sandbox") === false) {
        // We were not sent in a private (or sandbox) channel
        $response["channel_id"] = $container->get(MattermostAPI::class)->getChannelFromUserId($_REQUEST['user_id'] ??
            null) ?? "u91xx4ewa7dx3nf33ojzzi9i9c";
        $response["extra_responses"][] = [
            "text" => "**ERROR**: Please run the `/doc` command on a private chat, or in the ~sandbox channel."
        ];
    }
}

// This page's URL
$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

/**
 * Current part or option
 * @var Part|Option
 */
$current = (isset($_REQUEST['selection'])) ? $options[$_REQUEST['selection']] : $parts[0];

if (empty($current->children)) {
    // Ooops! No children for current. Move to the next part.
    $index = array_search($current->part, $parts) + 1;
    if (!isset($parts[$index])) {
        $current = null;
    } else {
        $current = $parts[$index];
    }
}

/**
 * Current progress in choices
 * @var Option[]
 */
$progress = array_map(function ($c) {global $options; return $options[(int)$c];},
    array_filter(explode(',', ($_REQUEST['progress'] ?? "")),
        function ($c) {return $c !== '';}));

/**
 * Generate output string based on choices
 */
// First, group the progress elements by part
$progressParts = [];
foreach ($progress as $progressItem) {
    $progressParts[array_search($progressItem->part, $parts)][] = $progressItem;
}
// Now we can generate the response
$result = "";
foreach ($progressParts as $id => $children) {
    /** @var Part $part */
    $part = $parts[$id];

    // Add part specifiers
    $result .= $part->prefix;
    // Add each child
    foreach ($children as $option) {
        /** @var Option $option */
        $result .= $option->value;
    }
    // Finish with part specifiers
    $result .= $part->postfix;
}

// Generate the response text
$attachment["text"] = "#### :page_with_curl: Documentation Numbering Helper\n\n";
if (!empty($progress)) {
    $back = array_slice($progress, 0, -1);
    $attachment["actions"][] = [
        "name" => "«",
        "integration" => [
            "url" => $url,
            "context" => (empty($back)) ? ["update" => true] : [
                "selection" => end($back)->id,
                "progress" => implode(',', array_map(function ($c) {return $c->id;}, $back))
            ]
        ]
    ];
}
if ($current !== null) {
    // We're not done yet
    if (!empty($result)) {
        $attachment["text"] .= "Current result:\n### {$result}";
    }
    $attachment["text"] .= "\n\n{$current->question}";

    foreach ($current->children as $child) {
        $attachment["actions"][] = [
            "name" => "{$child->value}: {$child->name}",
            "integration" => [
                "url" => $url,
                "context" => [
                    "selection" => $child->id,
                    "progress" => implode(',', array_map(function ($c) {return $c->id;},
                        array_merge($progress, [$child])))
                ]
            ]
        ];
    }
} else {
    // Get the next documentation number
    $registry = $container->get(App\Operations\Registry::class);
    $pdfParser = $container->get(PDFParser::class);
    $mattermost = $container->get(MattermostAPI::class);
    $ldap = $container->get(LDAP::class);

    // Format in the "-000" format
    $result .= "-" . $registry->getNextNumber($pdfParser->parseDocID($result));

    // Get the file for this
    $template = dirname($url) . "/template.php/$result.tex";

    // Append the full name of the user, if it can be found
    if ($username = $mattermost->getUsernameFromUserId($_REQUEST['user_id'] ?? 'unknown')) { // Query username from
        // Mattermost
        if ($fullName = $ldap->getCommonNameFromUsername($username)) { // Query full name from LDAP
            $template .= "?author=" . urlencode($fullName);
        }

        $cdrTemplate = $template . "&cdr";
    }

    $attachment["text"] .= "You are done! [Download your .tex file (_not_ for CDR)]($template)!";
    $attachment["text"] .= " For more information about the next steps, [read here](" . dirname($url) ."/docList/AcubeSAT-SYE-OA-001.pdf). \nThe **CDR template** can be [downloaded here :page_with_curl:]($cdrTemplate). The resulting documentation ID is:\n## {$result}\n";
    //$attachment["text"] .= "The following choices were made:\n";
    foreach ($progress as $option) {
        /** @var Option $option */
        $attachment["text"] .= "- `{$option->value}` {$option->name}\n";
    }

    $attachment["actions"][] = [
        "name" => "Start over",
        "integration" => ["url" => $url, "context" => ["update" => true]]
    ];
}

if (isset($_REQUEST['selection']) || isset($_REQUEST['update'])) {
    echo json_encode($response + [
        "update" => [ "response_type" => "in_channel", "message" => "", "type"=>"slack_attachment",
            // Do it the hard way, since "attachments" is not a default field for Posts
            "props" => [ "attachments" => [ $attachment ]]
        ],
    ] + (($current === null ? ["ephemeral_text" => "The result of the previous choice was:\n`{$result}`"] : [])));
} else {
    $response["attachments"] = [ $attachment ];
    echo json_encode($response);
}
file_put_contents("c", json_encode($attachment));
