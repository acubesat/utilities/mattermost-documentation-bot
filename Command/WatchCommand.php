<?php


namespace App\Command;


use App\Operations\Configuration;
use App\Operations\GoogleDriveClient;
use App\Operations\Registry;
use Exception;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Channel;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class WatchCommand extends Command
{
    protected static $defaultName = 'watch';

    /**
     * The configuration specified by the user
     * @var Configuration
     */
    private $config;

    /**
     * The PSR logger
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The Registry
     * @var Registry
     */
    private $registry;

    /**
     * WatchCommand constructor.
     *
     * @param array $configuration
     */
    public function __construct(Configuration $configuration, LoggerInterface $logger, Registry $registry)
    {
        parent::__construct();
        $this->config = $configuration;
        $this->logger = $logger;
        $this->registry = $registry;
    }


    protected function configure()
    {
        $this
            ->setDescription('Watches the specified directories.')
            ->setHelp('This command sends a request to google drive to watch the directories
specified in the configuration file. It needs to be run periodically, in order to
refresh the watch.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('Starting watcher script');

        try {
            $client = GoogleDriveClient::getGoogleClient($input, $output);
            $service = new Google_Service_Drive($client);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        if (!$service) throw new \RuntimeException("Could not start google service");

        $options = new Google_Service_Drive_Channel();
        $options->setType('web_hook');
        $options->setAddress($this->config->url . '/gdrivehook.php');
        $options->setId(uniqid('gdrmm'));
        // Set expiration to a bit less than 1 day
        $options->setExpiration( (time() + 60 * 60 * (60 - 1) * 24) * 1000 );

        try {
            $response = $service->changes->getStartPageToken();
            $this->logger->debug('Storing start page token ' . $response->getStartPageToken());
            $this->registry->setParameter('startPageToken', $response->getStartPageToken());

            $response = $service->changes->watch($response->getStartPageToken(), $options);

            $this->logger->debug('Watch will expire in ' . ($response->getExpiration() / 1000.0 - time()) . ' seconds from now.');
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
