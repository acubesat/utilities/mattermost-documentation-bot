<?php


namespace App\Command;


use App\Operations\Configuration;
use App\Operations\GoogleDrive;
use App\Operations\GoogleDriveClient;
use DI\Container;
use Exception;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Channel;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Dumper\YamlReferenceDumper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class DumpConfigCommand extends Command
{
    protected static $defaultName = 'dev:dump-config';

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * DebugCommand constructor.
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        parent::__construct();
        $this->configuration = $configuration;
    }


    protected function configure()
    {
        $this
            ->setDescription('Shows an example configuration file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dumper = new YamlReferenceDumper();

        $output->writeln($dumper->dump($this->configuration));
    }
}
