<?php


namespace App\Command;


use App\Operations\Configuration;
use App\Operations\GoogleDrive;
use App\Operations\GoogleDriveClient;
use DI\Container;
use Exception;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Channel;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class DebugCommand extends Command
{
    protected static $defaultName = 'dev:debug';

    /**
     * @var Container
     */
    private $container;

    /**
     * DebugCommand constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct();
        $this->container = $container;
    }


    protected function configure()
    {
        $this
            ->setDescription('Runs a debugging console');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Staring interactive shell...");

        // Prepare some useful variables
        $container = $this->container;
        $config = $this->container->get(Configuration::class);

        eval(\Psy\sh());
    }
}
