<?php


namespace App\Command;


use App\Chat\MattermostNotifications;
use App\Operations\Configuration;
use App\Operations\GoogleDrive;
use App\Operations\GoogleDriveClient;
use App\Operations\Registry;
use DI\Container;
use Exception;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Channel;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Yaml\Yaml;

class AnnounceCommand extends Command
{
    protected static $defaultName = 'announce';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MattermostNotifications
     */
    private $mattermostNotifications;

    /**
     * The documentation file registry
     * @var Registry
     */
    private $registry;

    /**
     * Path to the report PDF registry file
     */
    private const REGISTRY_LOCATION = __DIR__ . '/../config/registry.yml';

    /**
     * AnnounceCommand constructor.
     * @param LoggerInterface $logger
     * @param MattermostNotifications $mattermostNotifications
     */
    public function __construct(LoggerInterface $logger, MattermostNotifications $mattermostNotifications, Registry $registry)
    {
        parent::__construct();
        $this->logger = $logger;
        $this->mattermostNotifications = $mattermostNotifications;
        $this->registry = $registry;
    }


    protected function configure()
    {
        $this
            ->setDescription('Announces a documentation document on its respective channel, if possible')
            ->addArgument('docid', InputArgument::REQUIRED, 'The DocID of the documentation to announce')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Whether to announce this, even if it has been announced before')
            ->addOption('sandbox', 's', InputOption::VALUE_NONE, 'Whether to announce this in a sandbox rather than on the normal channel');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $docid = $input->getArgument('docid');

        $this->logger->notice("Asked to announce $docid");

        // Search for the documentation file
        $file = $this->registry->getEntryByDocID($docid);

        if ($file === null) {
            // File not found, an invalid document was given
            $this->logger->error("Unable to find documentation $docid for announcing");
            return;
        }

        if ((!isset($file['pendingAnnouncement']) || !$file['pendingAnnouncement']) && !$input->getOption('force')) {
            $this->logger->warning("Asked to announce $docid that has already been announced");
            return;
        }

        // Now we can do the announce
        $this->mattermostNotifications->announce($file, $input->getOption('sandbox'));

        // Clean up the file
        unset($file['pendingAnnouncement']);
        $this->registry->updateEntry($file);
    }
}
