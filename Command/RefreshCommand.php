<?php


namespace App\Command;


use App\Operations\Configuration;
use App\Operations\GoogleDrive;
use App\Operations\GoogleDriveClient;
use App\Operations\Registry;
use Exception;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Channel;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class RefreshCommand extends Command
{
    protected static $defaultName = 'refresh';

    /**
     * The configuration specified by the user
     * @var Configuration
     */
    private $config;

    /**
     * The PSR logger
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The file registry
     * @var Registry
     */
    private $registry;

    /**
     * The Google Drive operational utilities
     * @var GoogleDrive
     */
    private $drive;

    /**
     * Refresh constructor.
     */
    public function __construct(Configuration $configuration, LoggerInterface $logger, GoogleDrive $drive, Registry $registry)
    {
        parent::__construct();
        $this->config = $configuration;
        $this->logger = $logger;
        $this->drive  = $drive;
        $this->registry = $registry;
    }


    protected function configure()
    {
        $this
            ->setDescription('Refreshes files from the specified directories')
            ->addArgument('directory|file', InputArgument::OPTIONAL,
                'The ID of the directory or file to refresh. If this is not provided, all directories will be scanned.')
            ->addOption('file-directory', null, InputOption::VALUE_NONE,
                'Scan the entire directory of this file, instead of just the file itself')
            ->addOption('force', 'f', InputOption::VALUE_NONE,
                'Whether to refresh all files, regardless if they were updated or not')
            ->addOption('force-notifications', 'F', InputOption::VALUE_NONE,
                'Whether to show notifications for all files, even if they have already been sent to the user')
            ->addOption('changes', 'c', InputOption::VALUE_NONE,
                'Whether to refresh files based on the list of latest changes from Google Drive')
            ->addOption('no-downloads', 'd', InputOption::VALUE_NONE,
                'Prevent downloading files from Google Drive');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->notice('Refresh triggered', ['fileid' => $input->getArgument('directory|file')]);

        $this->drive->setDownloadsEnabled(!$input->getOption('no-downloads'));

        try {
            if ($input->getOption('changes')) {
                $this->logger->debug("Refreshing changes...");
                $this->drive->getChanges($input->getOption('force'));
            } else if ($id = $input->getArgument('directory|file')) {
                // Sanity check to prevent accessing forbidden areas
                if (in_array($id, iterator_to_array($this->config->watch->directories), true)) {
                    if (!$input->getOption('file-directory')) {
                        $this->logger->debug("Refreshing directory $id...");
                        $this->drive->getFileChanges($id, $input->getOption('force'));
                    } else {
                        throw new \InvalidArgumentException("You can only provide a file to be scanned with --file-directory.");
                    }
                } else {
                    if (!$input->getOption('file-directory')) {
                        $this->logger->debug("Refreshing file $id...");
                        $this->drive->getSingleFileChanges($id, $input->getOption('force'));
                    } else {
                        // Get the file's directory
                        if ($entry = $this->registry->getEntryByGoogleId($id)) {
                            $directory = $entry['directory'];
                            $this->logger->debug("Refreshing directory $directory with file $id...");
                            $this->drive->getFileChanges($directory, $input->getOption('force'));
                        }
                    }
                }
            } else {
                // Search all directories
                foreach ($this->config->watch->directories as $id) {
                    $this->logger->debug("Refreshing directory $id...");
                    $this->drive->getFileChanges($id, $input->getOption('force'));
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }

        $this->logger->info("Explicit refresh completed.");
    }
}
