<?php

// Receives a request from Mattermost, and performs the corresponding refresh

require_once __DIR__ . '/vendor/autoload.php';

header("Content-Type: application/json");
echo json_encode([
    'ephemeral_text' => 'Refreshing file...'
]);

$data = json_decode(file_get_contents('php://input'), true);

$id = trim($data['context']['id']); // Sanity check
if (strlen($id) > 254 || strlen($id) < 5 || !ctype_print($id)) die(); // Sanity check

if ($_SERVER['REQUEST_METHOD'] !== 'POST') die(); // Only POST requests

system('php ' . __DIR__ . '/console.php refresh ' . escapeshellarg($id) . ' --file-directory &');

