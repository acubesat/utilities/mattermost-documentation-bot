<?php


namespace App\Parser;

use App\Operations\Configuration;
use App\Operations\DocID;
use App\Operations\Exceptions\UserParseException;
use Imagick;
use PhpParser\Comment\Doc;
use Psr\Log\LoggerInterface;
use Smalot\PdfParser\Parser;
use Spatie\PdfToImage\Pdf;
use Symfony\Component\Yaml\Yaml;

/**
 * A Parser for PDF file names and metadata attributes
 *
 * @package App\Parser
 */
class PDFParser
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The DOC-ID stucture
     * @var DocID
     */
    private $docid;

    /**
     * The Smalot PDF Parser
     * @var \Smalot\PdfParser\Parser
     */
    private $parser;

    /**
     * @var XMPParser
     */
    private $xmpParser;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * PDFParser constructor.
     */
    public function __construct(LoggerInterface $logger, DocID $docid, Parser $parser, XMPParser $xmpParser,
                                Configuration $configuration)
    {
        $this->logger = $logger;
        $this->docid = $docid;
        $this->parser = $parser;
        $this->xmpParser = $xmpParser;
        $this->configuration = $configuration;
    }

    /**
     * Find out whether this parser supports this kind of file
     * @param $filename
     * @return bool
     */
    public function supportsFile($filename) {
        return (boolean) preg_match('/\.pdf$/i', $filename);
    }

    /**
     * @param $filename
     * @throws UserParseException
     */
    public function parseFilename($filename) {
        if (!$this->supportsFile($filename)) {
            throw new \RuntimeException("Unsupported error thrown into " . static::class);
        }

        preg_match('/^(.+?)(?:\.([^.]*)$|$)/', $filename, $matches);
        $baseFilename = $matches[1];
        $extension = $matches[2];

        $return = [];

        // Split the filename into parts
        $rootParts = preg_split('/\s+/', $baseFilename, 2);

        $return += $this->parseDocID($rootParts[0], true);

        if (isset($rootParts[1]) && !empty($rootParts[1])) {
            // We take the title from the filename, if it exists
            $return['title'] = $rootParts[1];
        }

        return $return;
    }

    /**
     * Generate an entry given the data in a DocID
     * @param string $docid The DocID
     * @param bool $strict Whether to throw a UserParseException whenever a DOC-ID rule is broken
     * @return array The entry
     */
    public function parseDocID(string $docid, bool $strict = false)
    {
        $return = [];

        // Split the docid into parts
        $parts = explode('-', $docid);

        if (count($parts) != 4 && $strict) {
            throw new UserParseException("DocIDs should be in the \"AcubeSAT-###-##-###\" format. You only have " . count($parts) . "/4 parts in your DocID.");
        }

        if ($parts[0] !== 'AcubeSAT' && $strict) {
            throw new UserParseException("The first part of your DocID should be \"AcubeSAT\"");
        }

        $return['docid'] = $docid;

        if ($this->docid->getSubsystem($parts[1]) === null && $strict) {
            throw new UserParseException("Unknown subsystem \"{$parts[1]}\"");
        }
        if (isset($parts[1])) $return['subsystem'] = $parts[1];

        // Parse category letters
        $return['categories'] = [];
        $categories = (isset($parts[2])) ? str_split($parts[2]) : [];
        // Check the categories
        $this->docid->getCategories($categories, $strict);
        $return['categories'] = array_slice($categories, 0, 10); // Sanity check to prevent huge arrays

        if (isset($parts[3])) $return['number'] = $parts[3];

        return $return;
    }

    /**
     * Parse a PDF file and get useful metadata from it
     *
     * @param string $data The binary PDF content
     * @param array $metadata The metadata of the file
     * @return array The data itself (not merged with $metadata)
     * @throws \Exception
     */
    public function parseData(string $data, $metadata = [])
    {
        $return = [];

        // Parse the XMP data first
        $xmp = $this->xmpParser->parseXMP($data); // Initial parsing of XMP data
        $this->logger->debug(($xmp ? count($xmp, COUNT_RECURSIVE) : 0) . ' XMP attributes found');
        if ($xmp === null) {
            // No XMP found
            if (((new \DateTime($metadata['created'])) > $this->configuration->strictDate)
                && (!DocID::isSubcategoryOf($metadata['categories'], $this->configuration->lenientCategory))
            ) {
                // The document was created after the strict date; XMP should exist; Show an error
                throw new UserParseException("Your document is in **draft" .
                    "** mode.\nPlease write `\documentclass[final]{cubedoc}` on the beginning of your document before uploading it to Google Drive." .
                    "\nOf course, you are free to upload new versions later if needed.");
            }
        } else {
            $return = $xmp;
        }

        try {
            $pdf = $this->parser->parseContent($data);

            $pages = $pdf->getPages();
            $details = $pdf->getDetails(true);
            if (isset($details['Author'])) $return['author'] = trim($details['Author']);

            $return['pages'] =
                $details['Pages'] ?? count($pages); // Do not trust the page count, in case the parsing is
            // wrong

            // Find the starting page of the document
            $pageCount = $return['pages'];
            $startingPage = 0;
            for ($p = 0; $p <= $pageCount / 2; $p++) { // Only search up to half of the document; that's where the
                // starting page should be
                $text = $pages[$p]->getText();

                // Search for keywords
                if (mb_stripos($text, 'This is the latest version of this document') !== false) {
                    $startingPage = $p + 1;
                    $this->logger->debug("Found DocID version notice in PDF");
                    break;
                }
                if (mb_stripos($text, 'changelog') !== false) {
                    $startingPage = $p;
                    $this->logger->debug("Found changelog in PDF");
                    break;
                }
                if (mb_stripos($text, 'table of contents') !== false) {
                    $startingPage = $p;
                    $this->logger->debug("Found table of contents in PDF");
                    break;
                }
            }

            // The page where the content starts
            $return['startPage'] = $startingPage;
            $this->logger->debug("Guessed starting page \"$startingPage\" for this document");
        } catch (\Exception $e) {
            $this->logger->warning("Could not parse PDF to get thumbnail", [
                'message' => $e->getMessage(),
                'type' => get_class($e)
            ]);
        }

        try {
            // Now, get the PDF image
            $this->logger->debug("Starting PDF image conversion");
            $file = tempnam(sys_get_temp_dir(), 'pdf') . '.pdf';
            file_put_contents($file, $data);

            $target = __DIR__ . '/../images/' . 'thumbnail' . bin2hex(random_bytes(10)) . '.png';
            $imagick = new \Imagick();
            $imagick->setResolution(40, 40); // Resolution in DPI
            $imagick->readImage($file . "[" . ($startingPage ?? 0) . "]");
            $imagick->setFormat('png');
            $imagick->setImageBackgroundColor('white');
            $imagick->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            $imagick->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
            $imagick->writeImage($target);
            $imagick->destroy();
            $imagick->clear();

            $return['thumbnail'] = 'images/' . basename($target);
            $this->logger->debug("Stored image in $target");

            return $return;
        } catch (\Exception $e) {
            $this->logger->warning("Could not get thumbnail from PDF", [
                'message' => $e->getMessage(),
                'type' => get_class($e)
            ]);
        }
    }
}
