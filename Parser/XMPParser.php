<?php


namespace App\Parser;

use App\Operations\DocID;
use App\Operations\Exceptions\UserParseException;
use Psr\Log\LoggerInterface;
use Sabre\Xml\Service as Xml;

/**
 * A class that gets XMP data from a PDF document
 */
class XMPParser
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DocID
     */
    private $docID;

    public function __construct(LoggerInterface $logger, DocID $docID)
    {
        $this->logger = $logger;

        // Add RDF namespaces because EasyRdf requires this to fetch entries using their XML tag names
        \EasyRdf_Namespace::set("xmp", "http://ns.adobe.com/xap/1.0/");
        \EasyRdf_Namespace::set("xmpMM", "http://ns.adobe.com/xap/1.0/mm/");
        \EasyRdf_Namespace::set("AcubeSAT", "http://helit.org/acubesat/ns/1.0#");
        \EasyRdf_Namespace::set("stVer", "http://ns.adobe.com/xap/1.0/sType/Version#");

        $this->docID = $docID;
    }

    /**
     * Get the parsed XMP data from a PDF document
     * @param $content
     * @return \EasyRdf_Graph|null
     * @throws \EasyRdf_Exception
     */
    public function getXMP($content)
    {
        // Search the location of the <rdf:RDF> tag in the PDF
        $lastOffset = -1;

        // For each occurrence of <rdf:RDF
        while (true) {
            $lastOffset = $xmp_data_start = strpos($content, '<rdf:RDF', $lastOffset + 1);
            $xmp_data_end = strpos($content, '</rdf:RDF>', $xmp_data_start);

            if ($xmp_data_start === false || $xmp_data_end === false) {
                // XMP data was not found at all
                return null;
            }

            // Some XMP data was found
            $xmp_length = $xmp_data_end - $xmp_data_start;
            $xmp_data = substr($content, $xmp_data_start, $xmp_length + 10);

            // Search for AcubeSAT in the XMP, to make sure we have the correct XMP that corresponds to a modern
            // document's metadata, and not some XMP of multimedia within the document
            if (strpos($xmp_data, 'xmlns:AcubeSAT') !== false) {
                // The data was found!
                break;
            } // else, keep searching
        }

        $rdf = new \EasyRdf_Graph('http://www.w3.org/1999/02/22-rdf-syntax-ns#'); // A random URI
        $rdf->parse($xmp_data);

//        file_put_contents('data.html', $rdf->dump());

        return $rdf;
    }

    /**
     * Parse the XMP data in a PDF document, and return the new metadata entry
     * @param $content string The PDF file entries
     * @return array The metadata
     */
    public function parseXMP($content)
    {
        $metadata = [];

        try {
            /** @var \EasyRdf_Graph $graph */
            $rdf = $this->getXMP($content);
            $uri = 'http://www.w3.org/1999/02/22-rdf-syntax-ns'; // The base URI of all properties

            // No XMP data was found
            if (!$rdf) return null;

            // If there is a well specified title, add it
            foreach ($rdf->get($uri, 'dc11:title') ?? [] as $title) {
                /** @var \EasyRdf_Literal $title */
                $metadata['title'] = $title->getValue();
            }

            // Document authors
            foreach ($rdf->get($uri, 'dc11:creator') ?? [] as $creator) {
                /** @var \EasyRdf_Literal $creator */
                $metadata['authors'][] = $creator->getValue();
            }
            if (empty($metadata['authors'])) {
                throw new UserParseException("Your document contains no authors. Please add at least one author, and separate different authors using multiple \\author{} commands.");
            }

            // Document changelog
            foreach ($rdf->get($uri, 'xmpmm:Versions') ?? [] as $version) {
                /** @var \EasyRdf_Resource $version */

                $versionData = [
                    'version' => $version->getLiteral('stVer:version'),
                    'date' => $version->getLiteral('stVer:modifyDate'),
                    'status' => $version->getLiteral('stVer:status'),
                ];

                foreach ($versionData as $key => &$value) {
                    if (!$value) {
                        $this->logger->warning("There is no $key for version", $versionData);
                    } else {
                        $value = $value->getValue();
                    }
                }
                unset($value); // Make sure the reference doesn't get used below
                if (isset($metadata['versions'][$versionData['version']])) {
                    throw new UserParseException("You have specified a version more than once.");
                }
                $metadata['versions'][$versionData['version']] = $versionData;

                if (!in_array($versionData['status'], $this->docID->getValidVersionTypes())) {
                    throw new UserParseException("Your versions contain an unknown version type. The only acceptable types are the following: " .
                        implode(', ', $this->docID->getValidVersionTypes()) .
                        ". Make sure they are in CAPITAL LETTERS.");
                }
            }
            if (empty($metadata['versions'])) {
                throw new UserParseException("Your document contains no versions. Please add some using the \\changelog{} command.");
            }

            // Simple literal values
            $literals = [
                'version' => 'xmpMM:VersionID',
                'public' => 'AcubeSAT:Public',
                'confidential' => 'AcubeSAT:Confidential',
                'templateVersion' => 'AcubeSAT:TemplateVersion'
            ];
            foreach ($literals as $mtdKey => $xmpKey) {
                if ($literal = $rdf->getLiteral($uri, $xmpKey)) {
                    $metadata[$mtdKey] = $literal->getValue();

                    // Cast boolean values
                    if ($metadata[$mtdKey] === 'True') {
                        $metadata[$mtdKey] = true;
                    } elseif ($metadata[$mtdKey] === 'False') {
                        $metadata[$mtdKey] = false;
                    }
                }
            }
        } catch (\EasyRdf_Exception $e) {
            $this->logger->error('Invalid XMP', [
                'error' => $e->getMessage(),
            ]);
        }

        return $metadata;
    }
}
