<?php

use App\Chat\MattermostAPI;
use App\Operations\Configuration;
use App\Operations\DirectAccessToken;
use App\Operations\DocID;
use App\Operations\GoogleDrive;
use App\Operations\Kernel;
use App\Operations\Registry;
use Gitlab\Model\Tag;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use PhpParser\Comment\Doc;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;
use Slim\Http\Body;
use Slim\Http\Headers;
use Slim\Http\RequestBody;
use Slim\Http\StatusCode;
use Slim\Route;
use Slim\Router;
use Slim\Views\PhpRenderer;

require __DIR__ . '/../vendor/autoload.php';

// The container
$container = Kernel::getWebContainer();

// Some often-used container definitions
$registry = $container->get(Registry::class);
$docid = $container->get(DocID::class);
$app = $container->get(Slim\App::class);
$view = $container->get(PhpRenderer::class);
$configuration = $container->get(Configuration::class);
$mattermost = $container->get(MattermostAPI::class);

if ($configuration->debug) {
    // Show debugging information
    ini_set("display_errors", "On");
    error_reporting(E_ALL | E_DEPRECATED | E_STRICT);
}

// Pass some global parameters to the view
$view->setAttributes([
    'container' => $container,
    'registry' => $registry,
    'docid' => $docid,
    'app' => $app
]);

if ($configuration->mattermost->oauth->enabled) {
    $provider = $container->get(GenericProvider::class);

    // Add authentication middleware
    $app->add(function (Request $request, Response $response, $next) use ($provider, $registry, $configuration) {
        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }

        /** @var AccessToken $accessToken */
        $accessToken = $_SESSION['accessToken'] ?? null;

        if ($request->getAttribute('public') || $request->getAttribute('route')->getName() === 'public') {
            // Requested only public documentation
            $registry->setFilter('public');
            return $next($request, $response);
        } elseif (isset($request->getQueryParams()['token'])) {
            $userToken = $request->getQueryParams()['token'];

            // Check for the existence of a correct token in the token list
            foreach ($configuration->tokens->login as $token) {
                if (hash_equals($token, $userToken)) {
                    // Store the access token, permanent log-in
                    $_SESSION['accessToken'] = new DirectAccessToken($token);

                    // Remove the 'token=' part from the URL, since we have stored it into the session
                    /** @var Slim\Http\Uri $url */
                    $url = $request->getUri();
                    $query = $request->getQueryParams();
                    unset($query['token']);

                    return $response->withRedirect($url->withQuery(http_build_query($query)), StatusCode::HTTP_TEMPORARY_REDIRECT);
                }
            }

            // TODO: Better error handling
            $response->getBody()->write('Invalid token provided.');
            return $response->withStatus(StatusCode::HTTP_FORBIDDEN);
        } elseif ($accessToken && !$accessToken->hasExpired()) {
            // A valid token exists; continue with the request
            return $next($request, $response);
        } elseif ($request->getAttribute('route')->getName() === 'auth') {
            // Allow access to the /auth route for login
            return $next($request, $response);
        } else {
            // Redirect the user to the oauth2 login
            $_SESSION['oauth2state'] = $provider->getState();
            $_SESSION['redirect'] = $request->getUri();

            /** @var \Slim\Http\Response $response */
            return $response->withRedirect($provider->getAuthorizationUrl(),
                StatusCode::HTTP_TEMPORARY_REDIRECT);
        }
    });

    $app->get('/auth', function (Request $request, Response $response) use ($container, $provider, $mattermost, $configuration) {
        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' => $request->getQueryParams()['code'] ?? null
        ]);

        $container->get(LoggerInterface::class)->debug('User authenticated with access token ' .
            substr($accessToken->getToken(), 0, 3) . ' ...');

        // Get the user's participating teams
        $user = $provider->getResourceOwner($accessToken)->toArray();

        if (!in_array($user['username'], iterator_to_array($configuration->mattermost->allowed_users))) {
            // User is not allowed by username, they may be allowed by team
            $teams = $mattermost->getTeamsFromUserId($user['id']);

            if (empty(array_intersect($teams, iterator_to_array($configuration->mattermost->allowed_teams)))) {
                // User is not allowed by team
                $response->getBody()->write('User not allowed to access documentation.');
                return $response->withStatus(StatusCode::HTTP_FORBIDDEN);
            }
        }

        // The session has already started, thanks to the middleware
        $_SESSION['accessToken'] = $accessToken;

        // Redirect the user to the intended URL
        $redirect = $_SESSION['redirect'] ?? null;
        $_SESSION['redirect'] = null; // Reset the redirect
        /** @var \Slim\Http\Response $response */

        if ($redirect) {
            // Intended redirect exists, move the user there
            return $response->withRedirect($redirect, StatusCode::HTTP_TEMPORARY_REDIRECT);
        } else {
            // By default, redirect to the / page
            return $response->withRedirect($container->get(Router::class)->pathFor('list'),
                StatusCode::HTTP_TEMPORARY_REDIRECT);
        }
    })->setName('auth');
}

// Middleware that removes the trailing slash from requests
$app->add(function (Request $request, Response $response, callable $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        // permanently redirect paths with a trailing slash
        // to their non-trailing counterpart
        $uri = $uri->withPath(substr($path, 0, -1));

        if($request->getMethod() == 'GET') {
            return $response->withRedirect((string)$uri, 301);
        }
        else {
            return $next($request->withUri($uri), $response);
        }
    }

    return $next($request, $response);
});

$app->add(function ($request, $response, $next) use ($container) {
    // Store some objects in the container
    $container->set(Request::class, $request);
    $container->set(Route::class, $request->getAttribute('route'));
    if (!$container->has('assetPath')) {
        // Set the base path for assets based on the first request
        $container->set('assetPath', $request->getUri()->getBasePath() . "/../");
    }

    return $next($request, $response);
});

$app->get('/public[/{rest}]', function (Request $request, Response $response, $args) use ($app, $container) {
    $rest = $args['rest'] ?? '';

    $uri = $request->getUri();
    $basePath = $uri->getBasePath() . "/public";
    $uri = $uri->withBasePath($basePath)->withPath($rest);

    // TODO: Use a subrequest instead
    return $app->process($request->withUri($uri)->withAttribute('public', true), $response);
})->setName('public');

$app->get('/template/changelog',
    function (Request $request, Response $response) use ($container, $configuration, $view) {
        $client = $container->get(\Gitlab\Client::class);
        $client->authenticate($configuration->gitlab->token, \Gitlab\Client::AUTH_URL_TOKEN);

        $repo = new \Gitlab\Model\Project($configuration->gitlab->template_repo, $client);

        $currentTag = -1;

        $tags = $repo->tags();
        $allCommits = $repo->commits(['per_page' => 100]);
        $commits = [];

        foreach ($allCommits as $commit) {
            if ($commit->id === ($tags[$currentTag + 1]->commit->id ?? null)) {
                $currentTag += 1;
            } else {
            }

            $commits[$currentTag][] = $commit;
        }

        // Create a new "empty" tag
        $tags[-1] = new \RecursiveArrayObject([
            'name' => "Development"
        ]);

        ksort($tags); // This is slow

        return $view->render($response, 'changelog.php', [
            'tags' => $tags,
            'commits' => $commits
        ]);
    })->setName('template_changelog');

$app->get('/categories', function (Request $request, Response $response) use ($view, $registry) {
    // Array counting each of the categories
    $counts = [];

    // Fetch all the entries from the registry
    $entries = array_filter($registry->getAllEntries(), function ($entry) {
        return $entry['valid'];
    });

    foreach ($entries as $entry) {
        for ($i = 1; $i <= count($entry['categories']); $i++) { // For every category letter
            $cat = implode(array_slice($entry['categories'], 0, $i)); // The categories ID as a string
            $counts[$cat] = ($counts[$cat] ?? 0) + 1; // Increase the categories ID counter
        }
    }
    $counts['total'] = count($entries);

    return $view->render($response, 'categories.php', [
        'counts' => $counts,
        'page' => 'categories'
    ]);
})->setName('categories');

$app->get('/{docid:AcubeSAT-.*}.pdf', function (Request $request, Response $response, $args) use (
    $container,
    $view,
    $registry
) {
    $id = $args['docid']; // The document's DocID

    $entry = $registry->getEntryByDocID($id);
    if (!$entry) {
        throw new NotFoundException($request, $response);
    }

    // Download the file from the cache
    $file = $container->get(\App\Operations\PDFCache::class)->get($entry);
    $body = new Body($file);

    $response = new \Slim\Http\Response(StatusCode::HTTP_OK,
        new Headers([
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . addslashes($entry['filename']) . '"',
            'Pragma' => 'public',
            'Cache-Control' => 'private, max-age=0, must-revalidate'
        ]), $body);

    return $response;
})->setName('openPdf');

$app->get('/{docid:AcubeSAT-.*}', function (Request $request, Response $response, $args) use (
    $view,
    $registry
) {
    $id = $args['docid']; // The document's DocID

    $entry = $registry->getEntryByDocID($id);
    if (!$entry) {
        throw new NotFoundException($request, $response);
    }

    // We have the entry, time to show it
    return $view->render($response, 'show.php', [
        'entry' => $entry
    ]);
})->setName('show');

$app->get('/[{filt1}[/{filt2}]]', function (Request $request, Response $response, $args) use (
    $view,
    $registry, $docid
) {
    $page = 'list';

    $entries = $registry->getAllEntries();
    if (array_key_exists('invalid', $request->getQueryParams())) {
        $page = 'invalid';
        $entries = array_filter($entries, function ($entry) {
            return !$entry['valid'];
        });
    } else {
        $entries = array_filter($entries, function ($entry) {
            return $entry['valid'];
        });
    }

    // Find if a filter is a subsystem
    $subsystem = null;
    if ($docid->getSubsystem($args['filt1'] ?? null)) {
        $subsystem = $args['filt1']; // Subsystem found
        $entries = array_filter($entries, function ($entry) use ($subsystem) {
            return $entry['subsystem'] === $subsystem;
        });

        // By default, sort by title when inside a subsystem
        $sort = 'title';
    }

    // Find if a filter is a category
    $category = $args['filt2'] ?? null;
    if ($category === null && $subsystem == null) {
        $category = $args['filt1'] ?? null;
    }

    if ($category) {
        // Do the category filtering
        $categories = str_split($category);
        $entries = array_filter($entries, function ($entry) use ($categories) {
            // Entry categories should match the requested categories, up to the requested categories' length
            return array_slice($entry['categories'], 0, count($categories)) === $categories;
        });

        // By default, sort by docid when inside a category
        $sort = 'docid';
    }

    if ($sort = $request->getQueryParams()['sort'] ?? $sort ?? null) {
        usort($entries, $registry->getSorter($sort));
    }

    return $view->render($response, 'list.php', [
        'category' => $category,
        'entries' => $entries,
        'page' => $page,
        'sort' => $sort,
        'subsystem' => $subsystem
    ]);
})->setName('list');

$app->run();
