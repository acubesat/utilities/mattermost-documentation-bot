## mattermost-documentation-bot

This repository contains the internally used utility that manages
documentation on the AcubeSAT project. It includes:
- A web interface (`docList` directory)
- A console interface (`./console.php`)

and integrations with:
- [Mattermost](https://mattermost.com/) slash commands
- [Google Drive](https://drive.google.com/)
- LDAP with the AcubeSAT [LDAP schema](https://gitlab.com/acubesat/utilities/ldap-schema)
- [Gitlab](https://gitlab.com)

Find an example of its usage at:
https://helit.org/mm/docList/public

<div align="center">
![Screenshot of documentation list](docs/screenshot-1.png)

![Screenshot of documentation Mattermost command](docs/screenshot-2.png)
</div>

### Features
- Helper commands to **create** new documents
- Keeping **count** of generated documents, support of AcubeSAT's
  **Doc-ID** format
- **Storage** of all document info in an SQLite database
- **Notifications** when a new document is created/updated
- **Parsing** XMP metadata in PDF files for easier classification
- **Monitoring** of Google Drive folders for changes
- **Listing**, searching, sorting, displaying documentation on
  a web interface

This toolkit was presented in the [Open Source CubeSat Workshop 2019](https://indico.oscw.space/event/3/contributions/89/).
  
### Installation
1. Install [composer](https://getcomposer.org/)
2. Run `composer install`
3. Create the Mattermost slash command, incoming hook and OAuth2 integrations,
   pointing to this project
4. Create integrations for the required services
5. Create a configuration file `config/config.yml` based on `config/config.example.yml`
6. Create a cron job for the automated scheduler:
```cron
* * * * * cd /var/www/html/mm && vendor/bin/crunz schedule:run -q
```
7. Run `./console.php refresh`


### Integration setup
#### Google Drive
1. Create a project in the [Google API Console](https://console.developers.google.com/),
   with access to Google Drive
2. Create an OAuth 2.0 client ID for a console client.
   Download its JSON file and store it as `credentials.json` in the `config` directory.

The console will ask you to connect to the created app with Google Drive as
soon as you run any command. This data is stored automatically in the `config/token.json`
file.

The configuration will require you to specify directory IDs for specified paths.

#### Mattermost
You can provide the required data (specified in `config.php`) by creating:
- A Mattermost slash command, pointing towards `hook.php`
- A Mattermost incoming hook
- A Mattermost OAuth2 integration

You can also create Mattermost channels as needed by the configuration.

#### LDAP
The LDAP integration requires a read-only LDAP account in order to match e-mail
addresses with usernames and full names of users and uploaders.

#### GitLab
The GitLab integration shows a changelog of a specified repo, based on its
tags. You can create the requested token at https://gitlab.com/profile/personal_access_tokens.
