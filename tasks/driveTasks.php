<?php

use Crunz\Schedule;

$schedule = new Schedule();


$schedule->run('./console.php watch')
    ->everyTwentyThreeHours()
    ->description("Refreshing the watches on GDrive");

$schedule->run('./console.php refresh')
    ->everyThirtyMinutes()
    ->description("Explicit file refresh");

return $schedule;
