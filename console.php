#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use App\Chat\MattermostNotifications;
use App\Command\AnnounceCommand;
use App\Command\DebugCommand;
use App\Command\DumpConfigCommand;
use App\Command\RefreshCommand;
use App\Command\WatchCommand;
use App\Operations\Kernel;
use Monolog\ErrorHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

// Define the container
$container = Kernel::getConsoleContainer();

$application = new Application('AcubeSAT Documentation Bot', '0.1.0');
$application->setDispatcher($container->get(EventDispatcher::class));
$application->getDefinition()->addOption(
    new InputOption('no-notifications', 'N', InputOption::VALUE_NONE,
        'Disable sending Mattermost notifications for issues. ' .
        'Useful for testing.'));
$application->getDefinition()->addOption(
    new InputOption('no-global-notifications', 'G', InputOption::VALUE_NONE,
        'Disable sending Mattermost notifications for issues only in the global channels. ' .
        'Private messages are sent normally. Useful for testing.')
);

$application->add($container->get(WatchCommand::class));
$application->add($container->get(RefreshCommand::class));
$application->add($container->get(AnnounceCommand::class));

if (function_exists('\\Psy\\sh')) {
    // Debugging commands
    $application->add($container->get(DebugCommand::class));
    $application->add($container->get(DumpConfigCommand::class));
}

// Add event to disable Mattermost messages
$container->get(EventDispatcher::class)
    ->addListener(ConsoleEvents::COMMAND, function (ConsoleCommandEvent $e) use ($container) {
        $container->get(MattermostNotifications::class)
            ->setEnabled(!$e->getInput()->getOption('no-notifications'))
            ->setEnabledGlobal(!$e->getInput()->getOption('no-global-notifications'));
    });

$application->run();
