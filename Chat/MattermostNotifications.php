<?php


namespace App\Chat;


use App\Operations\Configuration;
use App\Operations\DocID;
use App\Operations\LDAP;
use App\Parser\PDFParser;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class MattermostNotifications
{
    /**
     * The global configuration
     * @var Configuration
     */
    private $configuration;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var DocID
     */
    private $docID;

    /**
     * @var LDAP
     */
    private $ldap;

    /**
     * Whether Mattermost notifications will be sent
     * @var bool
     */
    private $enabled = true;

    /**
     * Whether Mattermost notifications will be sent to informative channels
     * @var bool
     */
    private $enabledGlobal = true;

    /**
     * MattermostNotifications constructor.
     * @param Configuration $configuration
     * @param LoggerInterface $logger
     * @param Client $client
     * @param DocID $docID
     * @param LDAP $ldap
     */
    public function __construct(Configuration $configuration, LoggerInterface $logger, Client $client, DocID $docID,
                                LDAP $ldap)
    {
        $this->configuration = $configuration;
        $this->logger = $logger;
        $this->client = $client;
        $this->docID = $docID;
        $this->ldap = $ldap;
    }

    /**
     * @param bool $enabled
     * @return MattermostNotifications
     */
    public function setEnabled(bool $enabled): MattermostNotifications
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @param bool $enabledGlobal
     * @return MattermostNotifications
     */
    public function setEnabledGlobal(bool $enabledGlobal): MattermostNotifications
    {
        $this->enabledGlobal = $enabledGlobal;
        return $this;
    }

    /**
     * Given a user's google drive e-mail, get their Mattermost username
     * @param $email
     * @return string
     */
    private function getUsername($email) {
        return $this->ldap->getUsernameFromEmail($email);
    }

    /**
     * Sends an augmented payload to the Mattermost hook
     *
     * @param $payload array
     * @param $email string The email of the user to send to
     */
    protected function send($payload, $email = null)
    {
        // Disabled; send nothing
        if (!$this->enabled) return;

        if (!$email) {
            $channel = null;
        } elseif ($email[0] === '~') {
            // Sending to channel
            $channel = substr($email, 1);
        } else {
            $username = $this->getUsername($email);
            if ($username !== null) {
                $channel = '@' . $username;
            } else {
                $this->logger->debug('Not sending message to user', ['email' => $email]);
                return; // Failed to send message, user not found
            }
        }

        $this->logger->info("Sending Mattermost message to $channel");

        $payload = array_replace([
            'channel' => $channel,
            'username' => 'Documentation Helper',
            'icon_url' => 'https://helit.org/mm_icon_doc_id_large.png'
        ], $payload);

        $this->client->post($this->configuration->mattermost->incoming_hook, ['json' => $payload]);
    }

    /**
     * Sends a notification to interested users
     *
     * @param $payload array
     */
    protected function sendNotification($payload)
    {
        if (!$this->enabled) return;
        if (!$this->enabledGlobal) return;

        $payload = array_replace([
            'username' => 'Documentation Helper\'s Assistant',
            'icon_url' => 'https://helit.org/mm_icon_doc_id_large.png'
        ], $payload);

        foreach ($this->configuration->mattermost->notifications as $recipient) {
            $this->logger->debug("Sending informational notification to $recipient");

            $payload['channel'] = $recipient;

            $this->client->post($this->configuration->mattermost->incoming_hook, ['json' => $payload]);
        }
    }

    /**
     * Make a string safe for markdown viewing
     * @param $string
     * @return string|string[]|null
     */
    protected function escape($string) {
        return preg_replace('/\*\~\#\/\[\]`/', '', $string);
    }

    public function docIdDeleted($email, $message, $metadata = []) {
        if (isset($metadata['creator'][0])) {
            $creator = $this->ldap->getCommonNameFromEmail($metadata['creator'][0], true);
        } else {
            $creator = '*Unknown*';
        }

        $this->sendNotification([
            'text' => "**#DocumentationDeleted** :x: $message
            
            Uploader: $creator
            Filename: [" . ($metadata['filename'] ?? '') . "](" . ($metadata['links']['view'] ?? $metadata['links']['download'] ?? '#') . ")
            "
        ]);
    }

    /**
     * Sends a message notifying the user for an error in the submitted documentation
     *
     * @param $email string The gdrive e-mail of the user to send the message to
     * @param $message string The message string to show to the user
     * @param $metadata array The metadata of the failed file
     */
    public function docIdError($email, $message, $metadata = []) {
        $name = $this->escape($metadata['filename'] ?? ' ');
        $creator = $this->ldap->getCommonNameFromEmail($metadata['creator'][0] ?? null, true);

        $this->send([
            'attachments' => [ [
                'fallback' => "Error occurred on documentation $name: $message",
                'pretext' => 'An **error** occurred while reading your new documentation file **' . $name . '**. Please fix it so that I don\'t complain!',
                'color' => '#dd5505',
                'title' => "Error!",
                'title_link' => $metadata['url'] ?? null,
                'text' => "$message",
                'actions' => [
                    [
                        'name' => ':checkered_flag: I fixed the file, please check it again',
                        'integration' => [
                            'url' => $this->configuration->url . '/refreshHook.php',
                            'context' => [
                                'action' => 'refresh',
                                'id' => $metadata['id']
                            ]
                        ]
                    ]
                ]
            ] ]
        ], $email);

        $this->sendNotification([
            'text' => "**#DocumentationError** :no_entry:
            
            Uploader: $creator
            Filename: [{$metadata['filename']}](" . ($metadata['links']['view'] ?? $metadata['links']['download'] ?? '#') . ")
            Error: $message
            "
        ]);
    }

    /**
     * Send a message with further information when a DocID submission was successful
     *
     * @param $email
     * @param array $metadata
     */
    public function docIdSuccess($email, $metadata) {
        $name = $this->escape($metadata['filename'] ?? ' ');
        $title = $this->escape($metadata['title'] ?? ' ');
        $subsystem = $this->docID->getSubsystem($metadata['subsystem']);
        $categories = implode(', ', iterator_to_array($this->docID->getCategories($metadata['categories'])));
        $docid = $metadata['docid'];
        $creator = $this->ldap->getCommonNameFromEmail($metadata['creator'][0] ?? null, true);
        $url = $metadata['links']['view'] ?? '#';
        $version = $metadata['version'] ?? '0.0';

        $this->send([
            'attachments' => [ [
                'fallback' => "New documentation $docid uploaded",
                'color' => '#22fa44',
                'title' => "New Documentation $docid",
                'title_link' => $metadata['links']['view'] ?? null,
                'text' => <<<EOD
You uploaded a new documentation file, with DocID `$docid` - here's some information:\n
- **Title**: $title
- **Subsystem**: $subsystem
- **Categories**: $categories
- **# ID**: `{$metadata['number']}`, Version $version
- **Filename**: [{$metadata['filename']}]($url)

Would you like me to notify your channel for this?
EOD
                ,'actions' => [
                    [
                        'name' => 'Yes, send a message to ~' . $this->configuration->mattermost->channels[$metadata['subsystem']],
                        'integration' => [
                            'url' => $this->configuration->url . '/announceHook.php',
                            'context' => [
                                'action' => 'announce',
                                'docid' => $docid
                            ]
                        ]
                    ]
                ]
            ] ]
        ], $email);

        $this->sendNotification([
            'text' => "**#DocumentationCreated** :white_check_mark:
            
            Uploader: $creator
            Filename: [{$metadata['filename']}](" . ($metadata['links']['view'] ?? $metadata['links']['download'] ?? '#') . ")
            Date: {$metadata['created']}, modified on {$metadata['modified']}
            "
        ]);
    }

    /**
     * Announce a document to the subsystem's channel
     */
    public function announce($metadata, $sandbox = false) {
        $name = $this->escape($metadata['filename'] ?? ' ');
        $title = $this->escape($metadata['title'] ?? ' ');
        $subsystem = $this->docID->getSubsystem($metadata['subsystem']);
        $categories = implode(', ', iterator_to_array($this->docID->getCategories($metadata['categories'])));
        $docid = $metadata['docid'];
        $user = $this->getUsername($metadata['creator'][0]);
        $channel = ($sandbox) ? $this->configuration->mattermost->sandbox_channel :
            $this->configuration->mattermost->channels[$metadata['subsystem']];

        $this->send([
            'text' => <<<EOD
**#Report**

:page_with_curl: Ο/Η @$user ανέβασε ένα νέο $categories report `$docid` στο Drive, με τίτλο:

#### $title

{$metadata['links']['view']}
EOD
        ], "~$channel");
    }

    /**
     * Announce a log entry to the administrative channel
     *
     * @param $level int
     * @param $message string
     * @param $channel string
     * @param $context array
     */
    public function infoLog($level, $message, $channel = null, $context = []) {
        $level = Logger::getLevelName($level);
        $this->sendNotification([
            'text' => "**#DocumentationLog** [$channel] **$level**
```php
$message
```"
        ]);
    }
}
