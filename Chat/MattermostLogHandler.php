<?php

namespace App\Chat;

use App\Chat\MattermostNotifications;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class MattermostLogHandler extends AbstractProcessingHandler
{
    private $enabled = true;

    /**
     * @var MattermostNotifications
     */
    private $mattermostNotifications;

    /**
     * List of log messages to transmit via Mattermost
     * @var string[]
     */
    private $messages;
    /**
     * The highest log level of messages sent
     * @var int
     */
    private $maxLevel = 0;
    /**
     * The context of all the messages
     * @var array[]
     */
    private $contexts;
    /**
     * The channel of the last log message handled
     * @var string
     */
    private $lastChannel;

    /**
     * MattermostLogHandler constructor.
     * @param \App\Chat\MattermostNotifications $mattermostNotifications
     */
    public function __construct(MattermostNotifications $mattermostNotifications, $level = Logger::DEBUG, $bubble =
    true)
    {
        parent::__construct($level, $bubble);
        $this->mattermostNotifications = $mattermostNotifications;
    }

    protected function write(array $record)
    {
        // Store this message so it can be sent in batch
        $this->messages[] = $record['formatted'];
        $this->maxLevel = max($this->maxLevel, $record['level']);
        $this->contexts[] = $record['context'];
        $this->lastChannel = $record['channel'];
    }

    public function close()
    {
        // Nothing to show
        if(empty($this->messages)) return;

        // Send the stored messages
        $this->mattermostNotifications->infoLog(
            $this->maxLevel,
            trim(implode("", $this->messages)),
            $this->lastChannel,
            $this->contexts
        );
    }
}
