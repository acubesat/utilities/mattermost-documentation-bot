<?php


namespace App\Chat;


use App\Operations\Configuration;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

class MattermostAPI
{
    /**
     * @var Client
     */
    private $guzzle;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * MattermostAPI constructor.
     * @param Client $guzzle
     * @param Configuration $configuration
     */
    public function __construct(Client $guzzle, Configuration $configuration, LoggerInterface $logger)
    {
        $this->guzzle = $guzzle;
        $this->configuration = $configuration;
        $this->logger = $logger;
    }

    /**
     * Get the user's username given the Mattermost database ID
     * @param string $id
     * @return string|null The username, or null if the user wasn't found
     */
    public function getUsernameFromUserId($id)
    {
        try {
            $request = $this->guzzle->get($this->configuration->mattermost->url . "/api/v4/users/" . urlencode($id), [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->configuration->mattermost->token,
                ],
            ]);

            $user = json_decode($request->getBody()->getContents(), true);
            return $user['username'];
        } catch (RequestException $e) {
            $this->logger->warning('Unable to find user for ID', [
                'id' => $id,
                'response' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
            return null;
        }
    }

    /**
     * Get the user's teams given the Mattermost database ID
     * @param string $id
     * @return string[]|null The team names, or null if the user wasn't found
     */
    public function getTeamsFromUserId($id)
    {
        try {
            $request = $this->guzzle->get($this->configuration->mattermost->url . "/api/v4/users/" . urlencode($id) . "/teams", [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->configuration->mattermost->token,
                ],
            ]);

            $teams = json_decode($request->getBody()->getContents(), true);
            return array_column($teams, 'name');
        } catch (RequestException $e) {
            $this->logger->warning('Unable to find user for ID', [
                'id' => $id,
                'response' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
            return null;
        }
    }

    /**
     * Create a direct message channel between a service account and a user
     * @param string $id The ID of the one participant in the direct message channel
     * @return string|null The channel ID, or null if an error occurred
     */
    public function getChannelFromUserId($id)
    {
        try {
            // First, get the user id of the token owner (service account)
            $request = $this->guzzle->get($this->configuration->mattermost->url . "/api/v4/users/me", [
                'headers' => ['Authorization' => 'Bearer ' . $this->configuration->mattermost->token],
            ]);
            $user = json_decode($request->getBody()->getContents(), true);
            $serviceId = $user['id'];

            // Second, create the channel
            $request = $this->guzzle->post($this->configuration->mattermost->url . "/api/v4/channels/direct", [
                'headers' => ['Authorization' => 'Bearer ' . $this->configuration->mattermost->token],
                'json' => [
                    $serviceId, $id
                ]
            ]);
            $channel = json_decode($request->getBody()->getContents(), true);

            return $channel['id'];
        } catch (RequestException $e) {
            $this->logger->warning('Unable to find channel for ID', [
                'id' => $id,
                'response' => json_decode($e->getResponse()->getBody(), true),
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
            return null;
        }
    }
}
