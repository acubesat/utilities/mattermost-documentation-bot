<?php

// File that runs after every google drive update
// Called automatically by the google drive hook

use App\Operations\Configuration;
use App\Operations\Kernel;

require __DIR__ . '/vendor/autoload.php';

file_put_contents('/var/www/html/mm/fffff', print_r($_SERVER, true));

$id = ($_SERVER['HTTP_X_GOOG_RESOURCE_ID'] ?? null);

if (strlen($id) > 254) die("\nDone"); // Sanity check
echo htmlspecialchars($id);

if (strlen($id) < 5) die("\nDone"); // Sanity check

$container = Kernel::getWebContainer();
// Only refresh if the watching is not disabled
if (!$container->get(Configuration::class)->watch->disable) {
    system('php ' . __DIR__ . '/console.php refresh --changes &');
}

echo "\nDone";
