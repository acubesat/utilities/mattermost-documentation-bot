<?php

$path = $_SERVER['PATH_INFO'];

preg_match("/\\/+([a-zA-Z0-9_\.\-]*?)\\.[^\\.]*/", $path, $matches);

if (!isset($matches[1])) die('Invalid DOC-ID');

$docid = substr(htmlspecialchars($matches[1]), 0, 30);

header('Content-Type: application/x-tex');

$file = file_get_contents(isset($_GET['cdr']) ? "template_CDR.tex" : "template.tex");

if (isset($_REQUEST['author'])) {
    // Character sanity check
    $author = preg_replace("/[#$%^&*+=\\-\\[\\]\\\\'\\/\\{\\}|:<>?]/", "", $_REQUEST['author']);
    $file = str_replace("AUTHOR 1", $author, $file);
    $file = str_replace("AUTHOR 2", "SECOND AUTHOR NAME HERE", $file);
} else {
    $file = str_replace("AUTHOR 1", "AUTHOR 1 NAME HERE", $file);
    $file = str_replace("AUTHOR 2", "AUTHOR 2 NAME HERE", $file);
}

$file = str_replace("TODAY", date('d/m/Y', time()), $file);
$file = str_replace("DOCID", $docid, $file);

echo $file;
