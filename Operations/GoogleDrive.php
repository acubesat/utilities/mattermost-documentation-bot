<?php


namespace App\Operations;

use App\Chat\MattermostNotifications;
use App\Operations\Exceptions\UserParseException;
use App\Parser\PDFParser;
use Exception;
use Google_Service_Drive;
use Google_Service_Drive_User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Performs basic operations on google drive files and directories
 *
 * Class GoogleDrive
 * @package App\Operations
 */
class GoogleDrive
{
    /**
     * @var GoogleDriveClient
     */
    private $client;

    /**
     * @var \Google_Client
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var PDFParser
     */
    private $pdfParser;

    /**
     * @var array
     */
    private $targetDirectoryCache = [];

    /**
     * @var MattermostNotifications
     */
    private $mattermostNotifications;

    /**
     * The parsed docid.yml file
     * @var array
     */
    private $docid;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var LDAP
     */
    private $ldap;

    /**
     * Whether to enable downloading the file from google drive to get more data
     * @var bool
     */
    private $downloadsEnabled = true;

    /**
     * Whether to send notifications for all scanned files, regardless if a file has been created or updated
     * (Default behaviour is to send only during the creation of a file)
     * @var bool
     */
    private $forcedNotificationsEnabled = true;

    /**
     * Path to the report PDF registry file
     */
    private const DOCID_LOCATION = __DIR__ . '/../docid.yml';

    /**
     * Maximum amount of files per page to fetch
     */
    private const PAGE_SIZE = 100;

    /**
     * Google drive file fields that contain the information we need
     */
    public const FIELDS = 'id, name, modifiedTime, createdTime, parents, owners, webViewLink, webContentLink, thumbnailLink, mimeType, size, trashed';

    /**
     * GoogleDrive constructor.
     *
     * @param \Google_Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(\Google_Client $client, LoggerInterface $logger, Configuration $configuration,
                                PDFParser $pdfParser, MattermostNotifications $mattermostNotifications, Registry
                                $registry, LDAP $ldap)
    {
        $this->logger = $logger;
        $this->configuration = $configuration;
        $this->pdfParser = $pdfParser;
        $this->mattermostNotifications = $mattermostNotifications;
        $this->docid = Yaml::parseFile(self::DOCID_LOCATION);
        $this->registry = $registry;
        $this->ldap = $ldap;

        try {
            $this->client = $client;
            $this->service = new Google_Service_Drive($client);
            $this->logger->info("Created client " . $this->client->getClientId());
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }


    }

    /**
     * Get the directory ID corresponding to a category in the target directory
     * @param $categories
     * @return string|null
     */
    private function getFileDirectory($categories)
    {
        // Join the $categories array into a single string for easier searching
        $joinedCats = join($categories);

        if (isset($this->targetDirectoryCache[$joinedCats])) {
            return $this->targetDirectoryCache[$joinedCats];
        }

        // We need to search or create the directory
        // The current category directory
        $currentDir = $this->configuration->target_directory;
        // The current DocID structure
        $currentDoc = $this->docid['categories'];
        foreach ($categories as $catId) {
            $catName = $currentDoc[$catId] ?? null;
            if ($catName === null) {
                // The category ID is supposed to be correct and checked
                $this->logger->error("Unable to find category $catId in registry");
                return null;
            }
            if (is_array($catName)) {
                $catName = $catName['name'];
            }

            // Get the children directories of this directory
            $optParams = [
                'pageSize' => 1,
                'fields' => 'nextPageToken, files(id, name)',
                'q' => "('$currentDir' in parents) and (mimeType = 'application/vnd.google-apps.folder') and (name = '" .
                    addslashes($catName) . "')"
            ];
            $subdirectories = $this->service->files->listFiles($optParams);
            if (count($subdirectories) > 1) {
                $this->logger->warning("Too many subdirectories ", array_map(function ($d) {
                    return [$d->getId(), $d->getName()];
                }, $subdirectories->getFiles()));
            }

            if (count($subdirectories) === 0) {
                // No subdirectories found; let's create one!
                $this->logger->debug("Creating subdirectory \"$catName\"");
                $newDirectory = $this->service->files->create(new \Google_Service_Drive_DriveFile([
                    'name' => $catName,
                    'mimeType' => 'application/vnd.google-apps.folder',
                    'parents' => [$currentDir]
                ]), ['fields' => 'id']);
                $currentDir = $newDirectory->getId();
                $this->logger->debug("Created subdirectory with ID $currentDir");
            } else {
                $currentDir = $subdirectories->current()->getId();
            }

            // Prepare values for the next iteration
            $currentDoc = $currentDoc[$catId]['categories'] ?? [];
        }

        //TODO: Cache single-letter directories as well, if needed
        $this->logger->debug("Found directory for \"$joinedCats\"");
        $this->targetDirectoryCache[$joinedCats] = $currentDir;

        return $currentDir;
    }


    /**
     * Get a list of modified documents, compared to the held list of documentation
     * reports
     *
     * @param $directoryId
     * @param $force bool Whether to change all files, regardless if they are updated now
     */
    public function getFileChanges($directoryId, bool $force = false)
    {
        // Sanitize the input
        $directoryId = preg_replace('/[^a-zA-Z0-9 _-]+/', '', $directoryId);

        if (!$this->validDirectory($directoryId)) {
            $this->logger->warning("Received update notification for unknown file $directoryId");
            return;
        }

        // First, get all the files in this directory
        //TODO: Don't get all those parameters unless necessary?
        $optParams = [
            'pageSize' => self::PAGE_SIZE,
            'fields' => 'nextPageToken, files(' . self::FIELDS . ')',
            'q' => "'$directoryId' in parents"
        ];
        $newFiles = $this->service->files->listFiles($optParams);

        $this->logger->debug('Found ' . count($newFiles) . ' files in directory');
        if (count($newFiles) >= self::PAGE_SIZE) {
            $this->logger->warning('More than ' . self::PAGE_SIZE . ' files might be in the directory');
        }

        // Make sure that all registry files in this directory were fetched; if not, then
        // some files were deleted
        foreach ($this->registry->getAllEntriesInDirectory($directoryId) as $entry) {
            // Search if we got this entry in the received list
            if (empty(array_filter(iterator_to_array($newFiles), function(\Google_Service_Drive_DriveFile $file) use ($entry) {
                return $file->id === $entry['id'];
            }))) {
                // While this file was stored as being in the directory, it was not found;
                // We can assume it was deleted.
                $this->logger->debug("Detected deleted file");
                $this->registry->removeEntry($entry['id']);
                $this->mattermostNotifications->docIdDeleted($entry['creator'][0], "File deleted", $entry);
            }
        }

        // Apply the processing for each file
        foreach ($newFiles as $file) {
            $this->getSingleFileChanges($file, $force);
        }
    }

    /**
     * Perform processing for a single file
     * @param $file array|string The file array or ID
     * @param $force bool Whether to change all files, regardless if they are updated now
     */
    public function getSingleFileChanges($file, bool $force = false)
    {
        if (!$file instanceof \Google_Service_Drive_DriveFile) {
            $file = $this->getFile($file);
        }

        /** @var \Google_Service_Drive_DriveFile $file */
        try {
            if (!$this->pdfParser->supportsFile($file->getName())) {
                // File not supported; we don't care
                return;
            }

            // Search for the file in the array
            $oldFile = $this->registry->getEntryByGoogleId($file->getId());
            if ($oldFile && $oldFile['modified'] === $file->getModifiedTime() && !$force && !$file->getTrashed()) {
                // File exists already and was not modified; we don't care
                return;
            }
            if (!$oldFile && $file->getTrashed()) {
                // File is trashed and does not exist in the registry; we don't care about it
                return;
            }

            $this->logger->info("Found updated file {$file->getName()}");

            $directoryId = $this->validFileDirectory($file);
            // Safety check that the file belongs in one of the predefined directories
            if ($directoryId === null) {
                $this->logger->error("File {$file->getId()} does not belong in one of the recognised directories.");
                return;
            }

            // Add some initial information for the file metadata. This information might be useful even earlier,
            // in case there is an error that needs to be reported
            $metadata = [
                'directory' => $directoryId,
                'filename' => $file->getName(),
                'creator' => array_map(function ($u) {
                    return $u->getEmailAddress();
                }, $file->getOwners()),
                'created' => $file->getCreatedTime(),
                'modified' => $file->getModifiedTime(),
                'id' => $file->getId(),
                'links' => [
                    'view' => $file->getWebViewLink(),
                    'download' => $file->getWebContentLink(),
                    'thumbnail' => $file->getThumbnailLink(),
                    'embedded' => str_replace('&export=download', '', $file->getWebContentLink())
                ],
                'size' => $file->getSize(),
                'valid' => true,
                'error' => null,
                'pendingAnnouncement' => true // TODO: Check if this works with an $oldFile check
            ];

            if ($metadata['size'] > 50 * 1024 * 1024) {
                // Too large file to preview; cancel some links
                unset($metadata['links']['view']);
                unset($metadata['links']['embedded']);
            }

            // Find if this file is trash
            if ($file->getTrashed()) {
                // The file was deleted by the user; make sure we delete it from the registry as well
                $this->logger->debug("Detected trashed file");
                $this->registry->removeEntry($file->getId());
                $this->mattermostNotifications->docIdDeleted($metadata['creator'][0], "File trashed", $metadata);
                return;
            }

            // Add the new metadata into the array
            $metadata = array_replace($metadata, $this->pdfParser->parseFilename($file->getName()));

            // Search for multiple occurrences of the same DocID
            $sameFile = current($this->registry->getEntriesWithSimilarDocIDs($metadata));
            if (!empty($sameFile)) {
                throw new UserParseException("File with DocID `{$metadata['docid']}` already exists:\n{$sameFile['docid']}" . " " .
                    ($sameFile['title'] ?? ""));
            }

            // Now parse the file itself
            if ($this->downloadsEnabled) {
                $this->logger->debug("Downloading file {$file->getName()} ...");
                $data = $this->downloadFile($file->getId());
                $metadata = array_replace($metadata, $this->pdfParser->parseData($data, $metadata));
            }

            // Some sanity checks for important parameters
            if (!isset($metadata['title'])) {
                $metadata['title'] = $metadata['filename'];
            }
            if (!isset($metadata['authors'])) {
                // Try to guess the document's creator given the email of the google drive creator
                if ($fullName = $this->ldap->getCommonNameFromEmail($metadata['creator'][0])) {
                    $metadata['authors'] = [$fullName];
                }
            }

            // Everything was successful; Notify the user
            if (!$oldFile || $this->forcedNotificationsEnabled) { // Only notify if the file was just added
                $this->mattermostNotifications->docIdSuccess($metadata['creator'][0], $metadata);
            }
        } catch (UserParseException $e) {
            $this->logger->notice("User failure detected for {$file->getName()}", [
                'message' => $e->getMessage(),
                'location' => $e->getFile() . ":" . $e->getLine()
            ]);
            // The user made a mistake when saving the file. Send them a notification on Mattermost
            $this->mattermostNotifications->docIdError($metadata['creator'][0] ?? null, $e->getMessage(),
                $metadata ?? []);

            $metadata['valid'] = false;
            $metadata['error'] = $e->getMessage();
        } catch (Exception $e) {
            $this->logger->critical("Unable to parse document {$file->getName()}: " . $e->getMessage(), [
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
            $metadata['valid'] = false;
            $metadata['error'] = $e->getMessage();
        }

        if (isset($metadata)) {
            // Everything was successful; add the metadata to the registry
            $this->registry->updateEntry($metadata);
            // Update even failed entries - even stuff such as the modification date may be useful
        }
    }

    /**
     * Get the files changed since the last list of changes
     * @param bool $force
     */
    public function getChanges(bool $force = false) {
        // Get the old token
        $token = $this->registry->getParameter('startPageToken');

        // Get the list of changes before the last fetch
        $response = $this->service->changes->listChanges($token, [
            'fields' => "newStartPageToken, changes(file(" . self::FIELDS . "))"]);

        foreach ($response->getChanges() as $change) {
            /** @var \Google_Service_Drive_Change $change */
            $this->logger->debug("Change found for " . $change->getFile()->getName());
            $file = $change->getFile();

            if ($file->getMimeType() == 'application/vnd.google-apps.folder') {
                // We don't care about changes in directories; just about changes in files
                continue;
            }

            if ($this->validFileDirectory($file)) {
                // Only get changes for files in a target directory. However, we are watching for changes in every
                // directory, and we should silentry ignore any other change.
                $this->getSingleFileChanges($file, $force);
            }
        }

        // Get a new token, and set that
        $this->registry->setParameter('startPageToken', $response->getNewStartPageToken());
        $this->logger->debug('Storing refreshed startPageToken ' . $response->getNewStartPageToken() . ' ' .
            $response->getNextPageToken());
    }

    /**
     * Get the array of a file's important data based on it ID
     * @return array
     */
    public function getFile($id) {
        try {
            return $this->service->files->get($id, ['fields' => self::FIELDS]);
        } catch (Exception $e) {
            $this->logger->critical("Unable to parse file {$id}: " . $e->getMessage());
        }

        return [];
    }

    /**
     * Finds out whether this file belongs in the list of watched files, and get the valid directory
     *
     * @return string|null The directory ID this file belongs to, or null if none is found
     */
    public function validFileDirectory(\Google_Service_Drive_DriveFile $file) {
        // Find the parents of the file that are not the target directory
        $parents = $file->getParents();
        $directoryId = array_find($parents ?? [], function($p) {
            return $p != $this->configuration->target_directory && $this->validDirectory($p);
        })
            ?: array_find($parents ?? [], function($p) {
                return $this->validDirectory($p);
            }); // ...or the target directory, if no other directory exists

        return $directoryId;
    }

    /**
     * Finds out whether this directory is included int he list of watched directories
     *
     * @param $id string The directory gdrive ID
     */
    public function validDirectory($id) {
        return in_array($id, $this->configuration->watch->directories->getArrayCopy(), true);
    }

    /**
     * Gets the contents of a PDF file based on its ID
     *
     * @param $id string The Google Drive ID of the file
     * @todo Limit the downloaded size
     * @return string The binary PDF contents
     */
    public function downloadFile($id) {
        $response = $this->service->files->get($id, ['alt' => 'media']);

        return $response->getBody()->getContents();
    }

    /**
     * Set downloads enabled
     * @param bool $downloadsEnabled
     * @return GoogleDrive
     */
    public function setDownloadsEnabled(bool $downloadsEnabled): GoogleDrive
    {
        $this->downloadsEnabled = $downloadsEnabled;
        return $this;
    }

    /**
     * @param bool $forcedNotificationsEnabled
     * @return GoogleDrive
     */
    public function setForcedNotificationsEnabled(bool $forcedNotificationsEnabled): GoogleDrive
    {
        $this->forcedNotificationsEnabled = $forcedNotificationsEnabled;
        return $this;
    }
}
