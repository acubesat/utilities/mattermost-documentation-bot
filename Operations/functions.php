<?php

/**
 * Returns the first item in an array that satisfies a condition
 *
 * @param $array array The array to search
 * @param $function callable A function that receives the (element, key) as arguments, and
 * should return exactly true for the predicate to succeed
 * @return mixed|null The value that satisfies the condition, or null if it is not found
 */
function array_find(array $array, callable $function) {
    foreach ($array as $key => $val) {
        if (call_user_func($function, $val, $key) === true)
            return $val;
    }
    return null;
}

/**
 * Returns the first item in an array that satisfies a condition
 *
 * @param $array array The array to search
 * @param $function callable A function that receives the (element, key) as arguments, and
 * should return exactly true for the predicate to succeed
 * @return mixed|null The key of the item that satisfies the condition, or null if it is not found
 */
function array_find_key(array $array, callable $function) {
    foreach ($array as $key => $val) {
        if (call_user_func($function, $val, $key) === true)
            return $key;
    }
    return null;
}
