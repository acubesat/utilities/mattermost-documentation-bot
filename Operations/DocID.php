<?php


namespace App\Operations;

use App\Operations\Exceptions\UserParseException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * The subsystems, categories and DocID production reference data
 *
 * Class DocID
 * @package App\Operations
 */
class DocID
{
    /**
     * The registry.yml file
     * @var array
     */
    private $docID;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Path to the report PDF registry file
     */
    private const DOCID_LOCATION = __DIR__ . '/../docid.yml';

    /**
     * Registry constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->docID = Yaml::parseFile(self::DOCID_LOCATION);
    }

    /**
     * Get the DocID reference data, as the processed YAML file
     * @return array
     */
    public function getDocID(): array
    {
        return $this->docID;
    }

    /**
     * Get a list of all the subsystem IDs
     * @return string[]
     */
    public function getSubsystems()
    {
        return array_keys($this->docID['subsystems']);
    }

    /**
     * Get all the subcategory IDs that belong in a category
     * @param array|null $top The parent path to start searching from, null to return the top-level categories first
     * @return array
     */
    public function getSubCategories(array $top = null)
    {
        $currentSubcategories = $this->docID['categories'];
        if ($top !== null) {
            foreach ($top as $id) {
                // We're done here, if this category does not exist
                if (!isset($currentSubcategories[$id])) break;

                // Prepare for the next iteration
                $currentSubcategories = $currentSubcategories[$id]['categories'] ?? [];
            }
        }

        return array_keys($currentSubcategories);
    }

    /**
     * Returns the name of a subsystem from the registry
     * @param $id string The subsystem alias
     * @param $short bool Whether to return the short name of this subsystem, if it exists
     * @return string|null The full subsystem name, or null if it was not found
     */
    public function getSubsystem($id, $short = false)
    {
        if ($short) {
            // Search the short names first, and then the full names if that doesn't succeed
            return $this->docID['short_subsystem_names'][$id] ?? $this->docID['subsystems'][$id] ?? null;
        } else {
            return $this->docID['subsystems'][$id] ?? null;
        }
    }

    /**
     * Returns the full names of some category IDs
     * @param array $ids The category IDs in hierarchical order
     * @param bool $strict Whether to throw an Exception if an unknown category is encountered
     * @return string[]
     */
    public function getCategories(array $ids, $strict = false): \Generator
    {
        $currentSubcategories = $this->docID['categories'];
        $pastIds = ''; // An array to hold the current sequence of IDs
        foreach ($ids as $id) {
            // We're done here, if this category does not exist
            if (!isset($currentSubcategories[$id])) {
                if ($strict) {
                    // Throw an error if needed
                    throw new UserParseException("Unknown category \"$id\"");
                } else {
                    // Otherwise, just return the current number of categories
                    break;
                }
            }

            $name = $currentSubcategories[$id];
            if (is_array($name)) $name = $name['name']; // The category was an array of its name and subcategories
            $pastIds .= $id;

            // Prepare for the next iteration
            $currentSubcategories = $currentSubcategories[$id]['categories'] ?? [];

            // Return one element
            yield $pastIds => $name;
        }
    }

    /**
     * Returns category IDs as a comma-separated list of their full names
     * @param array $ids The category IDs in hierarchical order
     * @return string
     */
    public function formatCategories(array $ids)
    {
        return implode(', ', iterator_to_array($this->getCategories($ids)));
    }

    /**
     * Get a list of valid version types that can appear in a document's changelog
     * @return string[]
     */
    public function getValidVersionTypes()
    {
        return $this->docID['versionTypes'];
    }

    /**
     * Get the description of the category
     * @param array|string $ids The category IDs
     * @return string|null The category description, or null if it doesn't exist
     */
    public function getCategoryDescription($ids)
    {
        if (is_string($ids)) {
            $ids = str_split($ids);
        }

        $currentSubcategories = $this->docID;

        foreach ($ids as $id) {
            // Prepare for the next iteration
            $currentSubcategories = $currentSubcategories['categories'] ?? [];

            // We're done here, if this category does not exist
            if (!isset($currentSubcategories[$id])) {
                // Throw an error if needed
                throw new \InvalidArgumentException("Unknown category \"$id\"");
            }

            $currentSubcategories = $currentSubcategories[$id];
            if (!is_array($currentSubcategories)) {
                // The current category can not contain a description
                return null;
            }
        }

        return $currentSubcategories['description'] ?? null;
    }

    /**
     * Find if a $subset category is the same or a child of a $superset category. For example:
     *  - "OL" is a subseteq of "OL"
     *  - "OL" is a subseteq of "O"
     *  - "OP" is not a subseteq of "OL"
     * @param $superset array|string The category IDs
     * @param $subset array|string The category IDs
     * @return boolean
     */
    public static function isSubcategoryOf($subset, $superset)
    {
        if ($superset === null || $subset === null) return false;

        if (is_array($subset)) $subset = implode($subset);
        if (is_array($superset)) $superset = implode($superset);

        return substr($subset, 0, strlen($superset)) === $superset;
    }
}
