<?php


namespace App\Operations;


use Adldap\Adldap;
use Adldap\Connections\Provider;
use App\Chat\MattermostLogHandler;
use Monolog\ErrorHandler;
use Psr\Log\LogLevel;
use function Clue\StreamFilter\fun;
use DI\Container;
use DI\ContainerBuilder;
use function DI\get;
use Google_Client;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use League\OAuth2\Client\Provider\GenericProvider;
use Monolog\Handler\FingersCrossedHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Route;
use Slim\Router;
use Slim\Views\PhpRenderer;
use Symfony\Bridge\Monolog\Formatter\ConsoleFormatter;
use Symfony\Bridge\Monolog\Handler\ConsoleHandler;
use Symfony\Component\Config\Definition\Dumper\YamlReferenceDumper;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Yaml\Yaml;

abstract class Kernel
{
    /**
     * Fill the container with the generic services, applicable both for web and on the console
     */
    private static function genericFillContainer(Container $container)
    {
        // Parse the configuration
        $container->set(Configuration::class, function () {
            $configurationTree = new Configuration([]);

            $filename = __DIR__ . '/../config/config.yml';
            if (!file_exists($filename)) {
                file_put_contents($filename, (new YamlReferenceDumper())->dump($configurationTree));
                die('A configuration file was created for you at ' . $filename .
                    ". Please fill in the values, and come back again!\n");
            }

            $config = Yaml::parseFile($filename);
            $configuration = (new Processor())->processConfiguration($configurationTree, $config);

            return new Configuration($configuration);
        });

        // Set up Registry
        $container->set(Registry::class, \DI\autowire(Registry\SqliteRegistry::class));

        // Set up Guzzle HTTP client
        $container->set(Client::class, function () use ($container) {
            $handlerStack = HandlerStack::create();
            $handlerStack->push(Middleware::log(
                $container->get(LoggerInterface::class)->withName('guzzle-http'),
                new MessageFormatter(MessageFormatter::SHORT),
                LogLevel::DEBUG
            ));

            return new Client([
                'handler' => $handlerStack
            ]);
        });

        // Set up Adldap LDAP client
        $container->set(Provider::class, function () use ($container) {
            $configuration = $container->get(Configuration::class);

            // Construct new Adldap instance.
            $ad = new Adldap();

            // Create a configuration array.
            $config = [
                'hosts' => [$configuration->ldap->host],
                'base_dn' => $configuration->ldap->baseDn,
                'username' => $configuration->ldap->username,
                'password' => $configuration->ldap->password,
            ];

            $ad->addProvider($config);

            try {
                // If a successful connection is made to your server, the provider will be returned.
                $provider = $ad->connect();

                $container->get(LoggerInterface::class)->debug('Connected to LDAP successfully');

                return $provider;
            } catch (\Adldap\Auth\BindException $e) {
                if (!$container->has(LoggerInterface::class)) {
                    throw $e;
                } else {
                    $container->get(LoggerInterface::class)->emergency('Could not connect to LDAP server',
                        [
                            'message' => $e->getMessage(),
                            'detailedError' => $e->getDetailedError()
                        ]
                    );
                }
            }
        });

        // LDAP helper operation class
        $container->set(LDAP::class, \DI\autowire()
            ->lazy()); // Lazy load LDAP so that we don't connect to the server
        // if we don't need to
    }

    /**
     * Gets a container, ready to prepare the web
     * @return Container
     */
    public static function getWebContainer()
    {
        $container = ContainerBuilder::buildDevContainer();
        static::genericFillContainer($container);

        // Create the logging facilities
        $container->set('logger', function () use ($container) {
            $logger = new Logger('acubesat-mm-doc-bot');
            $logger->pushHandler(new StreamHandler(__DIR__ . '/../config/doc-bot.log'));
            $logger->pushHandler(new StreamHandler(__DIR__ . '/../config/doc-bot.errors.log', Logger::WARNING));

            return $logger;
        });
        ErrorHandler::register($container->get('logger')->withName('php')); // Logger logs PHP errors as
        // well

        // Slim App
        $container->set(App::class, \DI\create()->constructor([
            'settings' => [
                'displayErrorDetails' => $container->get(Configuration::class)->debug,
                'determineRouteBeforeAppMiddleware' => true
            ]
        ]));
        $container->set(PhpRenderer::class, \DI\create()->constructor(__DIR__ . '/../views/'));
        $container->set(Router::class, function () {
            global $app;
            return $app->getContainer()->get('router');
        });

        $container->set(\Psr\Log\LoggerInterface::class, \DI\get('logger'));

        // OAuth
        $config = $container->get(Configuration::class);
        $container->set(GenericProvider::class, \DI\create()->constructor([
            'clientId' => $config->mattermost->oauth->client_id,
            'clientSecret' => $config->mattermost->oauth->client_secret,
            'redirectUri' => $config->url . '/docList.php/auth',
            'urlAuthorize' => $config->mattermost->url . '/oauth/authorize',
            'urlAccessToken' => $config->mattermost->url . '/oauth/access_token',
            'urlResourceOwnerDetails' => $config->mattermost->url . '/api/v4/users/me',
        ]));

        return $container;
    }

    /**
     * Gets a container, ready to prepare the console
     * @return Container
     */
    public static function getConsoleContainer()
    {
        $container = ContainerBuilder::buildDevContainer();
        static::genericFillContainer($container);

        // Create the logging facilities
        $container->set('logger', function () use ($container) {
            $logger = new Logger('acubesat-mm-doc-bot');
            $handler = new ConsoleHandler();
            $handler->setFormatter(new ConsoleFormatter());
            $container->get(EventDispatcher::class)->addSubscriber($handler);
            $logger->pushHandler($handler);
            //TODO: Set logging level for the stream handler
            $logger->pushHandler(new StreamHandler(__DIR__ . '/../config/doc-bot.log'));
            $logger->pushHandler(new StreamHandler(__DIR__ . '/../config/doc-bot.errors.log', Logger::WARNING));

            return $logger;
        });
        $container->set(\Psr\Log\LoggerInterface::class, \DI\get('logger'));
        // Add mattermost handler
        $container->get(LoggerInterface::class)->pushHandler(
            new FingersCrossedHandler($container->get(MattermostLogHandler::class), Logger::WARNING)
        );
        ErrorHandler::register($container->get(LoggerInterface::class)->withName('php')); // Logger logs PHP errors as
        // well

        // Set up Google Drive
        $container->set(Google_Client::class, function () use ($container) {
            return \App\Operations\GoogleDriveClient::getGoogleClient(new ArgvInput(), new ConsoleOutput());
        });

        return $container;
    }
}
