<?php


namespace App\Operations\Registry;

use App\Operations\Registry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * A registry for all the documentation files, that stores metadata about them in .yml format
 *
 * Attention: Do NOT use this in production environments, as it might be prone to conflicts and synchronization
 * issues between commands that are reading/writing to the registry at the same time
 *
 * Class YamlRegistry
 * @package App\Operations\Registry
 */
class YamlRegistry extends Registry
{
    /**
     * The documentation file registry
     * @var array
     */
    private $registry;

    /**
     * Path to the report PDF registry file
     */
    private const REGISTRY_LOCATION = __DIR__ . '/../../config/registry.yml';

    /**
     * Path to the key-value PDF registry file
     */
    private const REGISTRY_PARAMETERS_LOCATION = __DIR__ . '/../../config/registry.parameters.yml';

    /**
     * Registry constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);

        // Create the registries if necessary
        if (!file_exists(self::REGISTRY_LOCATION)) {
            $logger->notice("Creating new registry file " . self::REGISTRY_LOCATION);
            // Store an empty array that contains no registry entries
            file_put_contents(self::REGISTRY_LOCATION, '[]');
        }
        if (!file_exists(self::REGISTRY_PARAMETERS_LOCATION)) {
            $logger->notice("Creating new registry file " . self::REGISTRY_PARAMETERS_LOCATION);
            // Store an empty array that contains no registry entries
            file_put_contents(self::REGISTRY_PARAMETERS_LOCATION, '{}');
        }

        $this->registry = Yaml::parseFile(self::REGISTRY_LOCATION);
    }


    public function getEntryByDocId(string $docid)
    {
        $file = null;
        foreach ($this->registry as &$entry) {
            // Search all entries until the one with the docID is found
            if ($entry['docid'] === $docid) {
                $file = $entry;
                break;
            }
        }

        return $this->infuse($file);
    }

    public function getEntryByGoogleId(string $id)
    {
        // We can just search by key
        return $this->infuse($this->registry[$id] ?? null);
    }

    public function updateEntry(array $entry)
    {
        if (!isset($entry['id'])) {
            $this->logger->critical("Asked to update an entry, but I don't know its ID!", [ 'entry' => $entry ]);
            return;
        }

        // Update or create the entry
        $this->registry[$entry['id']] = $entry;

        // Store the new registry to the disc
        $this->logger->info("Updated entry in the registry", ['filename' => $entry['filename'] ?? null]);
        file_put_contents(self::REGISTRY_LOCATION, Yaml::dump($this->registry));
    }

    public function removeEntry(string $id)
    {
        unset($this->registry[$id]);

        $this->logger->info("Removed entry from the registry", ['id' => $id]);
        file_put_contents(self::REGISTRY_LOCATION, Yaml::dump($this->registry));
    }

    public function getEntriesWithSimilarDocIDs(array $entry)
    {
        return $this->infuseMany(array_filter($this->registry, function ($elem) use ($entry) {
            return $elem['categories'] === $entry['categories'] && $elem['number'] === $entry['number']
                && $elem['id'] !== $entry['id'];
        }));
    }

    public function getAllEntries()
    {
        return $this->infuseMany($this->registry);
    }

    public function getAllEntriesInDirectory(string $directory)
    {
        return $this->infuseMany(array_filter($this->registry, function($r) use ($directory) {
            return $r['directory'] === $directory;
        }));
    }

    public function getNextNumber(array $entry)
    {
        $max = 0;
        foreach ($this->registry as $registry) {
            if ($entry['categories'] == $registry['categories'] && $registry['number'] > $max) {
                $max = $registry['number'];
            }
        }

        return str_pad($max + 1, 3, "0", STR_PAD_LEFT);
    }

    public function getParameter(string $key)
    {
        $parameters = Yaml::parseFile(self::REGISTRY_PARAMETERS_LOCATION);
        return $parameters[$key] ?? null;
    }

    public function setParameter(string $key, $value)
    {
        $parameters = Yaml::parseFile(self::REGISTRY_PARAMETERS_LOCATION);
        $parameters[$key] = $value;
        file_put_contents(self::REGISTRY_PARAMETERS_LOCATION, Yaml::dump($parameters));
    }


}
