<?php


namespace App\Operations\Registry;


use App\Operations\Registry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

class SqliteRegistry extends Registry
{
    /**
     * The documentation file registry
     * @var \PDO
     */
    private $pdo;

    /**
     * Path to the report PDF registry file
     */
    private const REGISTRY_LOCATION = __DIR__ . '/../../config/registry.db';

    /**
     * Registry constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);

        $this->connect();
    }

    /**
     * Creates a connection to the PDO instance
     * @return \PDO
     */
    private function connect()
    {
        if ($this->pdo === null) {
            $this->pdo = new \PDO("sqlite:" . self::REGISTRY_LOCATION, null, null, [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            ]);
        }

        // Create a schema if it doesn't already exist
        $this->pdo->exec(<<<PDO
CREATE TABLE IF NOT EXISTS documentation (
    id TEXT UNIQUE,
    docid TEXT,
    directory TEXT,
    subsystem TEXT,
    category TEXT,
    number TEXT,
    version TEXT,
    filename TEXT,
    title TEXT,
    valid INT,
    date TEXT,
    data TEXT
);

CREATE TABLE IF NOT EXISTS parameters (
    key TEXT UNIQUE,
    value TEX
);
PDO
        );

        return $this->pdo;
    }

    public function getEntryByDocID(string $docId)
    {
        try {
            $query = $this->pdo->prepare("SELECT data FROM documentation WHERE docid = :docid LIMIT 1");
            $query->execute(['docid' => $docId]);
            $result = $query->fetchColumn();

            //TODO: On PHP 7.3, throw on Error
            return $this->infuse(json_decode($result, true));
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }

    public function getEntryByGoogleId(string $id)
    {
        try {
            $query = $this->pdo->prepare("SELECT data FROM documentation WHERE id = :id LIMIT 1");
            $query->execute(['id' => $id]);
            $result = $query->fetchColumn();

            //TODO: On PHP 7.3, throw on Error
            return $this->infuse(json_decode($result, true));
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }

    public function updateEntry(array $entry)
    {
        try {
            $query = $this->pdo->prepare("INSERT OR REPLACE INTO documentation(id, docid, directory, subsystem, category, number, version, filename, title, valid, date, data) VALUES
            (:id, :docid, :directory, :subsystem, :category, :number, :version, :filename, :title, :valid, :date, :data)");
            $query->execute([
                'id' => $entry['id'],
                'docid' => $entry['docid'] ?? null,
                'directory' => $entry['directory'] ?? null,
                'subsystem' => $entry['subsystem'] ?? null,
                'category' => implode($entry['categories'] ?? []),
                'number' => $entry['number'] ?? null,
                'version' => $entry['version'] ?? null,
                'filename' => $entry['filename'] ?? null,
                'title' => $entry['title'] ?? null,
                'valid' => $entry['valid'] ?? false,
                'date' => $entry['modified'] ?? null,
                'data' => json_encode($entry)
            ]);

            $this->logger->info("Updated entry in the registry", ['filename' => $entry['filename'] ?? null]);
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
        }
    }

    public function removeEntry($id)
    {
        try {
            $query = $this->pdo->prepare("DELETE FROM documentation WHERE id = :id");
            $query->execute([
                'id' => $id
            ]);

            $this->logger->info("Removed entry from the registry", ['id' => $id]);
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
        }
    }

    public function getEntriesWithSimilarDocIDs(array $entry)
    {
        try {
            $query =
                $this->pdo->prepare("SELECT data FROM documentation WHERE category = :category AND number = :number AND id != :id");
            $query->execute([
                'category' => implode($entry['categories']),
                'number' => $entry['number'],
                'id' => $entry['id']
            ]);
            $results = $query->fetchAll(\PDO::FETCH_COLUMN);

            //TODO: On PHP 7.3, throw on Error
            return array_filter(array_map(function ($r) {
                return $this->infuse(json_decode($r, true));
            }, $results));
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }

    public function getAllEntries()
    {
        try {
            $query =
                $this->pdo->prepare("SELECT data FROM documentation ORDER BY subsystem, category, number");
            $query->execute();
            $results = $query->fetchAll(\PDO::FETCH_COLUMN);

            //TODO: On PHP 7.3, throw on Error
            return array_filter(array_map(function ($r) {
                return $this->infuse(json_decode($r, true));
            }, $results));
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }

    public function getAllEntriesInDirectory(string $directory)
    {
        try {
            $query =
                $this->pdo->prepare("SELECT data FROM documentation WHERE directory = :directory ORDER BY subsystem, category, number ");
            $query->execute(['directory' => $directory]);
            $results = $query->fetchAll(\PDO::FETCH_COLUMN);

            //TODO: On PHP 7.3, throw on Error
            return array_filter(array_map(function ($r) {
                return $this->infuse(json_decode($r, true));
            }, $results));
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }

    public function getNextNumber(array $entry)
    {
        try {
            // Find the documentation with the highest number...
            $query =
                $this->pdo->prepare("SELECT number FROM documentation WHERE category = :category ORDER BY number DESC LIMIT 1");
            $query->execute(['category' => implode($entry['categories'])]);
            $max = $query->fetchColumn();

            // ..and add 1 to that number
            return str_pad($max + 1, 3, "0", STR_PAD_LEFT);
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }

    public function getParameter(string $key)
    {
        try {
            // Find the documentation with the highest number...
            $query =
                $this->pdo->prepare("SELECT value FROM parameters WHERE key = :key LIMIT 1");
            $query->execute(['key' => $key]);
            $value = $query->fetchColumn();

            return $value;
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }

    public function setParameter(string $key, $value) {
        try {
            // Find the documentation with the highest number...
            $query =
                $this->pdo->prepare("INSERT OR REPLACE INTO parameters(key, value) VALUES (:key, :value)");
            $query->execute(['key' => $key, 'value' => $value]);
        } catch (\Exception $e) {
            $this->logger->alert($e->getMessage());
            return null;
        }
    }
}
