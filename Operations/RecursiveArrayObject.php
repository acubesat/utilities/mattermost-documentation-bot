<?php


namespace App\Operations;

/**
 * Child of RecursiveArrayObject that doesn't mess with objects, just arrays
 */
class RecursiveArrayObject extends \RecursiveArrayObject
{
    public function __construct($input = null, $flags = self::ARRAY_AS_PROPS, $iterator_class = "ArrayIterator"){
        foreach($input as $k=>$v) {
            if (is_array($v))
                $this->offsetSet($k,(new RecursiveArrayObject($v, $flags)));
            else
                $this->offsetSet($k, $v);
        }
        return $this;
    }
}
