<?php


namespace App\Operations;


use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Models\ModelNotFoundException;
use Psr\Log\LoggerInterface;

class LDAP
{
    /**
     * @var Provider
     */
    private $ldap;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LDAP constructor.
     * @param Provider $ldap
     * @param LoggerInterface $logger
     */
    public function __construct(Provider $ldap, LoggerInterface $logger)
    {
        $this->ldap = $ldap;
        $this->logger = $logger;
    }

    /**
     * Given a user's google drive email, get their LDAP Entity
     * @param string $attribute
     * @param string $value
     * @return Model|null
     */
    protected function getUserByAttribute(string $attribute, string $value)
    {
        if ($attribute === null) return null; // Sanity check

        try {
            /** @var Model $model */
            $model = $this->ldap->search()->findByOrFail($attribute, $value);

            return $model;
        } catch (ModelNotFoundException $e) {
            $this->logger->error('Could not find user with attribute in LDAP directory', [
                $attribute => $value
            ]);

            return null;
        }
    }

    /**
     * Given a user's google drive email, get their LDAP username
     * @param string $email
     * @return string|null The username or null if not found
     */
    public function getUsernameFromEmail(string $email)
    {
        $user = $this->getUserByAttribute('xgrasatGdriveEmail', $email);
        return $user ? $user->getFirstAttribute('uid') : null;
    }

    /**
     * Given a user's google drive email, get their LDAP common name (i.e. full name)
     * @param string $email
     * @param boolean $default Whether to return the email by default, if the cn was not found
     * @return string|null The common name or null if not found
     */
    public function getCommonNameFromEmail(string $email, $default = false)
    {
        $user = $this->getUserByAttribute('xgrasatGdriveEmail', $email);
        return $user ? $user->getFirstAttribute('cn') : ($default ? $email : null);
    }

    /**
     * Given a user's username, get their LDAP common name (i.e. full name)
     * @param string $uid
     * @return string|null The common name or null if not found
     */
    public function getCommonNameFromUsername(string $uid)
    {
        $user = $this->getUserByAttribute('uid', $uid);
        return $user ? $user->getFirstAttribute('cn') : null;
    }
}

