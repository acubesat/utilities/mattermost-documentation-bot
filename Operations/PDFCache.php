<?php


namespace App\Operations;


use Carbon\Carbon;
use Psr\Log\LoggerInterface;

class PDFCache
{
    /**
     * @todo Use a more temporary directory for this?
     */
    private const CACHE_DIR = __DIR__ . "/../files/";

    private const CACHE_EXPIRY = '1 week';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PDFCache constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     *
     * @param array $metadata The metadata of the file from the Registry
     * @return resource The file stream. Don't forget to close it afterwards
     * @throws \Exception
     */
    public function get(array $metadata) {
        if (!isset($metadata['docid'])) {
            throw new \Exception("Asked to fetch a document from cache that contains no DocID");
        }

        $docid = $metadata['docid'];
        $filename = self::CACHE_DIR . $docid . '.pdf'; // The filename with which to store the file in the cache

        if (file_exists($filename)) {
            // Get file modification date
            $carbon = new Carbon();
            $timestamp = filemtime($filename);
            if ($timestamp === false) {
                throw new \Exception("Timestamp not found for file");
            }
            $timestamp = Carbon::createFromTimestamp($timestamp);

            if ($timestamp->isBefore(Carbon::now()->sub(self::CACHE_EXPIRY))) {
                // Cache has expired
                $upToDate = false;
                $this->logger->debug('Cache for file expired', ['filename' => $filename]);
            } else if ($timestamp->isBefore(new Carbon($metadata['modified']))) {
                // Document has updated and we need to fetch it again
                $upToDate = false;
                $this->logger->debug('Cache for file is outdated', ['filename' => $filename]);
            } else {
                // Cache exists and is recent
                $upToDate = true;
                $this->logger->debug('Cache for file found', ['filename' => $filename]);
            }
        } else {
            // File does not exist at all
            $upToDate = false;
            $this->logger->debug('No cache for file', ['filename' => $filename]);
        }

        if (!$upToDate) {
            // Cache needs to be refreshed; open the file
            return $this->update($metadata);
        } else {
            return fopen($filename, 'rb'); // read-only access to the file
        }
    }

    /**
     * Update a file in the cache
     * @param array $metadata
     * @return resource The file stream. Don't forget to close it afterwards.
     * @throws \Exception
     */
    protected function update(array $metadata) {
        if (!isset($metadata['docid'])) {
            throw new \Exception("Asked to fetch a document from cache that contains no DocID");
        }
        $docid = $metadata['docid'];
        $filename = self::CACHE_DIR . $docid . '.pdf'; // The filename with which to store the file in the cache

        $download = fopen($metadata['links']['download'], 'rb');
        $target = fopen($filename, 'w+b'); // w+ will remove contents already in the file

        stream_copy_to_stream($download, $target);

        fclose($download);
        return $target;
    }
}