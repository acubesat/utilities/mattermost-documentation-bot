<?php


namespace App\Operations;


use Psr\Log\LoggerInterface;
use Slim\Router;
use Symfony\Component\Yaml\Yaml;

abstract class Registry
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Map of all the doclist overrides
     * @var array docid => object array
     */
    private $overrides = [];

    /**
     * A map of all enabled filters
     */
    private $filters = [
        'public' => false
    ];

    /**
     * Path to the override registry file
     */
    private const OVERRIDES_LOCATION = __DIR__ . '/../config/registry.overrides.yml';

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        // Create the overrides if necessary
        if (!file_exists(self::OVERRIDES_LOCATION)) {
            $logger->notice("Creating new override file " . self::OVERRIDES_LOCATION);
            // Store an empty array that contains no override entries
            file_put_contents(self::OVERRIDES_LOCATION, '{}');
        }

        $this->overrides = Yaml::parseFile(self::OVERRIDES_LOCATION);
    }

    /**
     * Set the configuration for a filter
     *
     * * Currently acceptable filters:
     * - public => boolean (show only public documents)
     *
     * @param string $name The name of the filter
     * @param mixed $value The value to set the filter to
     */
    public function setFilter($name, $value = true) {
        $this->filters[$name] = $value;
    }

    /**
     * Returns one registry entry by its DocID
     * @param $docid
     * @return array|null The entry, or null if it was not found
     */
    abstract public function getEntryByDocID(string $docId);

    /**
     * Returns one registry entry by its Google Drive file ID
     * @param $id
     * @return array|null The entry, or null if it was not ofund
     */
    abstract public function getEntryByGoogleId(string $id);

    /**
     * Update or create a new entry in the registry
     * @param array $entry
     */
    abstract public function updateEntry(array $entry);

    /**
     * Permanently delete an entry from the registry
     * @param string $id The Google Drive file ID
     */
    abstract public function removeEntry(string $id);

    /**
     * Locate competing entries that have equal DocIDs, but don't have the same IDs
     * @param array $entry
     * @return array The list of competing entries
     */
    abstract public function getEntriesWithSimilarDocIDs(array $entry);

    /**
     * Returns all the entries in the registry
     * @return array[]
     */
    abstract public function getAllEntries();

    /**
     * Get all the entries in a directory
     * @param string $directory The google drive directory ID
     * @return array[]
     */
    abstract public function getAllEntriesInDirectory(string $directory);

    /**
     * Get the next documentation number
     *
     * @param array $entry The current entry
     * @return string
     */
    abstract public function getNextNumber(array $entry);

    /**
     * Get a parameter from the key-value database
     *
     * @param string $key The key of the parameter
     * @return mixed|null The parameter or null if it was not found
     */
    abstract public function getParameter(string $key);

    /**
     * Set a parameter in the key-value database
     *
     * Creates the parameter if it doesn't exist
     *
     * @param string $key
     * @param mixed $value
     */
    abstract public function setParameter(string $key, $value);

    /**
     * Get a function that sorts (based on the <=> operator) based on a parameter
     *
     * @param $parameter string The parameter key/column to sort on
     * @return \Closure
     */
    public function getSorter($parameter) {
        switch ($parameter) {
            case 'name':
                return function ($a, $b) {
                    return ($a['name'] ?? null) <=> ($b['name'] ?? null);
                };
            case 'title':
                return function ($a, $b) {
                    return (strtolower($a['title']) ?? null) <=> (strtolower($b['title']) ?? null);
                };
            case 'modified':
                return function ($a, $b) {
                    return ($b['modified'] ?? null) <=> ($a['modified'] ?? null);
                };
            case 'created':
                return function ($a, $b) {
                    return ($b['created'] ?? null) <=> ($a['created'] ?? null);
                };
            case 'author':
            case 'authors':
                return function ($a, $b) {
                    return ($a['creators'][0] ?? null) <=> ($b['creators'][0] ?? null);
                };
            case 'category':
            case 'categories':
                return function ($a, $b) {
                    if ($a['categories'] == $b['categories']) {
                        return ($a['docid'] ?? null) <=> ($b['docid'] ?? null);
                    }
                    return implode($a['categories'] ?? null) <=> implode($b['categories'] ?? null);
                };
            case 'docid':
                return function ($a, $b) {
                    $partsa = explode('-', $a['docid'] ?? '', 4);
                    $partsb = explode('-', $b['docid'] ?? '', 4);

                    // Keep the CATEGORY-ID part only
                    $parta = ($partsa[2] ?? '') . '-' . ($partsa[3] ?? '');
                    $partb = ($partsb[2] ?? '') . '-' . ($partsb[3] ?? '');

                    return $parta <=> $partb;
                };
            default:
                return function ($a, $b) {
                    if (($a['subsystem'] ?? null) == ($b['subsystem'] ?? null)) {
                        return implode($a['categories'] ?? null) <=> implode($b['categories'] ?? null);
                    }
                    return ($a['subsystem'] ?? null) <=> ($b['subsystem'] ?? null);
                };
        }
    }

    /**
     * Get a path that contains an embedded representation of this entry
     *
     * @param array $metadata The metadata of the file
     * @param Router $router The slim router that contains the request routes
     * @param string $basePath The base path of the project
     * @param bool $page Whether to redirect at the first page where there is meaningful content
     *
     * @return string|null
     */
    public static function getPdfjsUrl(array $metadata, Router $router, string $basePath, $page = true)
    {
        if (!isset($metadata['docid'])) {
            return null;
        }

        $url = $router->pathFor('openPdf', [
            'docid' => $metadata['docid']
        ]);

        $url = $basePath . '/assets/pdfjs/web/viewer.html?file=' . urlencode($url);

        if ($page && isset($metadata['startPage']) && $metadata['startPage'] !== 1) {
            $url .= '#page=' . ((int) ($metadata['startPage'] + 1)); // PDFs start counting pages from 1
        }


        return $url;
    }

    /**
     * Get a URL that requests an inline view of the PDF by the browser (natively, without pdf.js)
     * @param array $metadata The metadata of the file
     * @param bool $page Whether to redirect the user to a certain page
     * @param int $offset A number of pages to add to the final result
     * @return string|null The link, or null if it's not found
     */
    public static function getEmbeddedUrl(array $metadata, $page = true, $offset = 0) {
        if (isset($metadata['links']['embedded'])) {
            if ($page) {
                $page = $metadata['startPage'] ?? 0;
                $page += 1 + $offset; // Pages start counting from 0
                if ($page <= 1) $page = 1; // Make sure we start counting from 1

                return $metadata['links']['embedded'] . '#page=' . $page;
            } else {
                return $metadata['links']['embedded'];
            }
        } else {
            return null;
        }
    }

    /**
     * Apply some final processing to the returned metadata (e.g. apply overrides, clean up, rename entries)
     * @param array $metadata The original input metadata
     * @return array The processed metadata
     */
    protected function infuse($metadata)
    {
        if (!$metadata) return $metadata ;

        // Search for this entry in the overrides
        if (isset($metadata['docid']) && isset($this->overrides[$metadata['docid']])) {
            // Merge the array values. Overrides have higher priority.
            $metadata = array_merge($metadata, $this->overrides[$metadata['docid']]);
        }

        // Apply filters
        if ($this->filters['public']) {
            // Only show public documents
            if (!isset($metadata['public']) || $metadata['public'] != true || $metadata['public'] === "False") {
                // Document is not public, do not show
                return null;
            }
        }

        return $metadata;
    }

    /**
     * Apply some final processing to an array of returned metadata
     *
     * Applies Registry::infuse() to each one of the provided array values
     *
     * @param array $metadataList The original input metadata list
     * @return array The processed metadata list
     */
    protected function infuseMany(array $metadataList) {
        return array_map([$this, 'infuse'], $metadataList);
    }
}
