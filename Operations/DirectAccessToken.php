<?php


namespace App\Operations;


use League\OAuth2\Client\Token\AccessTokenInterface;
use League\OAuth2\Client\Token\ResourceOwnerAccessTokenInterface;
use RuntimeException;

/**
 * A class representing a token (stored in the session) for a direct string allowing the user to perform any action or
 * login directly via the URL.
 *
 * Class DirectAccessToken
 * @package App\Operations
 */
class DirectAccessToken implements AccessTokenInterface, ResourceOwnerAccessTokenInterface
{
    /**
     * The token string
     * @var string
     */
    private $token;

    /**
     * DirectAccessToken constructor.
     * @param string $token The token string
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }


    /**
     * Returns the access token string of this instance.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->getToken();
    }

    /**
     * Returns the refresh token, if defined.
     *
     * @return string|null
     */
    public function getRefreshToken()
    {
        return null;
    }

    /**
     * Returns the expiration timestamp, if defined.
     *
     * @return integer|null
     */
    public function getExpires()
    {
        return null;
    }

    /**
     * Checks if this token has expired.
     *
     * @return boolean true if the token has expired, false otherwise.
     * @throws RuntimeException if 'expires' is not set on the token.
     */
    public function hasExpired()
    {
        return false;
    }

    /**
     * Returns additional vendor values stored in the token.
     *
     * @return array
     */
    public function getValues()
    {
        return [];
    }

    /**
     * Returns a string representation of the access token
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getToken();
    }

    /**
     * Returns an array of parameters to serialize when this is serialized with
     * json_encode().
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'token' => $this->getToken(),
            'owner' => $this->getResourceOwnerId()
        ];
    }

    /**
     * Returns the resource owner identifier, if defined.
     *
     * @return string|null
     */
    public function getResourceOwnerId()
    {
        return null;
    }

}