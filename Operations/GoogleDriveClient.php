<?php


namespace App\Operations;


use Exception;
use Google\Auth\Credentials\ServiceAccountCredentials;
use Google_Client;
use Google_Service_Drive;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Helper functions for Google Drive integration
 *
 * @package App\Operations
 */
class GoogleDriveClient
{

    public static function getGoogleServiceAccountClient(InputInterface $input, OutputInterface $output)
    {
        $client = new Google_Client();
        $client->setApplicationName('AcubeSAT Documentation watcher');
        $client->setScopes([Google_Service_Drive::DRIVE]);
        $client->setAccessType('offline');

        $credential = new ServiceAccountCredentials([Google_Service_Drive::DRIVE],
            __DIR__ . '/../config/credentials.json',
            'electrovesta@gmail.com');
    }

    /**
     * Gets an authenticated Google API client
     * @param boolean $service Whether to initialise a service account rather than a regular account
     * @return Google_Client
     * @throws \Google_Exception
     */
    public static function getGoogleClient(InputInterface $input, OutputInterface $output)
    {
        $client = new Google_Client();
        $client->setApplicationName('AcubeSAT Documentation watcher');
        $client->setAuthConfig(__DIR__ . '/../config/credentials.json');
        $client->setScopes([Google_Service_Drive::DRIVE]);
        $client->setAccessType('offline');
        $client->setRedirectUri("urn:ietf:wg:oauth:2.0:oob");
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = __DIR__ . '/../config/token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();

                $formatter = new FormatterHelper();
                $questionHelper = new QuestionHelper();

                $formattedLine = $formatter->formatBlock([
                    'Open the following link in your browser:',
                    $authUrl
                ], 'info');
                $output->writeln($formattedLine);

                $question = new Question('Enter verification code: ');
                $authCode = trim($questionHelper->ask($input, $output, $question));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                if ($accessToken) {
                    $client->setAccessToken($accessToken);
                }

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }

        return $client;
    }
}
