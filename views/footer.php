<?php
use Slim\Router;
?>

<footer>
    <hr>
    <div class="uk-container uk-align-right uk-text-meta">
        <a href="https://gitlab.com/acubesat/utilities/mattermost-documentation-bot" 
            class="uk-link">Source Code @ Gitlab</a>
        &bull;
        <a href="<?php
        echo htmlentities($container->get(Router::class)->pathFor('template_changelog'));
        ?>" class="uk-link">Template Changelog</a>
    </div>
</footer>

<!-- JS FILES -->
<script src="<?php echo base_dir(); ?>assets/list.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit-icons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.14/lodash.js" integrity="sha256-/kJFk3HQgNKuH9RkXlRXcNwwoVw1zMMtk6KWiv0BUfg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fuse.js/3.4.5/fuse.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rangy/1.3.0/rangy-core.min.js" integrity="sha256-9hInzgauxBs2NY86rW7DD7zXeC2+Q9o6P4UZyF94I1s=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rangy/1.3.0/rangy-classapplier.min.js" integrity="sha256-cV+UNHAmL8mlH1C4d7NBZD38yNJQH3E9xCTplxbWb6k=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rangy/1.3.0/rangy-textrange.min.js" integrity="sha256-LL+AN5baYpThJZmPyycIy5QzhINYE3P3jjcshIcPDpw=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-lightbox@2.1.0/src/simpleLightbox.min.js"></script>
</body>
</html>
