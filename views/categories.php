<?php

use Slim\Router;

require __DIR__ . '/header.php';

$backgrounds = [
    'warning', 'primary', 'success', 'danger', 'secondary'
];
?>

<section class="uk-section uk-article">
    <div class="uk-container uk-container-small">
        <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">AcubeSAT Documentation
            Categories <span class="uk-text-meta uk-margin-left"><?php echo $counts['total']; ?></span>
            </h2>
    </div>

    <!-- large image -->
    <div class="uk-container uk-container-expand uk-section">
        <div class="uk-flex-center" uk-grid>
            <?php foreach ($docid->getSubCategories() as $top) { ?>
                <div
                        class="uk-card uk-card-body uk-card-default uk-card-hover uk-margin-top uk-margin-bottom
                uk-margin-left uk-margin-right">
                    <a href="<?php
                    echo htmlentities($container->get(Router::class)->pathFor('list', [
                            'filt1' => $top
                    ]));
                    ?>" class="uk-card-title uk-link-heading">
                        <span class="uk-text-meta uk-text-right uk-display-inline-block
                                    uk-margin-right"
                        ><?php echo $counts[$top] ?? 0 ?>
                                </span><?php echo $docid->getCategories([$top])->current();
                    ?></a>
                    <?php if ($description = $docid->getCategoryDescription($top)) { ?>
                        <p class="uk-text-muted db-category-description"><?php echo htmlentities($description); ?></p>
                    <?php } ?>
                    <ul class="uk-list uk-list-divider">
                        <?php foreach ($docid->getSubCategories([$top]) as $sub) { ?>
                            <li>
                                <a class="uk-link-text" href="<?php
                                    echo htmlentities($container->get(Router::class)->pathFor('list', [
                                        'filt1' => $top . $sub
                                ]));
                                ?>">
                                    <div class="uk-label uk-text-center uk-background-<?php echo current($backgrounds)
                                    ; ?>
                                    uk-text-small
                                    uk-padding-remove-horizontal"
                                         style="width: 2em; font-size: .8rem;"><?php
                                        echo $top . $sub;
                                    ?></div><span class="uk-text-meta uk-width-1-6 uk-text-right uk-display-inline-block
                                    uk-margin-right"
                                    ><?php echo $counts[$top . $sub] ?? 0 ?>
                                </span><?php
                                    $categories = iterator_to_array($docid->getCategories([$top, $sub]));
                                    echo end($categories);
                                ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <?php next($backgrounds); ?>
            <?php } ?>
        </div>
    </div>
</section>

<?php
require 'footer.php';
?>
