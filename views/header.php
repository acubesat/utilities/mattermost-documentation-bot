<?php
use App\Operations\DocID;use App\Operations\Registry;use Slim\Router;

/** @var Registry $registry */
/** @var DocID $docid */

require __DIR__ . '/helpers.php';

?><!DOCTYPE html>
<html lang="en" class="mm-doc-web">

<head>
    <?php
        // Create the title in parts
        $title = [ "Documentation" ];
        if (isset($page) && $page !== 'list') $title[] = ucfirst($page);
        if (isset($subsystem)) $title[] = $docid->getSubsystem($subsystem);
        if (isset($category)) $title[] = $docid->formatCategories(str_split($category));
        $title[] = "AcubeSAT";
    ?>

    <title><?php echo implode(' - ', $title); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/css/uikit.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_dir(); ?>assets/simpleLightbox.min.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_dir(); ?>apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_dir(); ?>favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_dir(); ?>favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_dir(); ?>site.webmanifest">

    <style>
        html.mm-doc-web {
            /* Make text darker by default */
            color: #444;
        }

        .uk-table thead .uk-link, .uk-table thead a {
            color: inherit;
        }

        .uk-background-success {
            background-color: #32d296 !important;
        }

        .uk-background-warning {
            background-color: #faa05a !important;
        }

        .uk-background-danger {
            background-color: #f0506e !important;
        }

        .db-subsystems {
            max-width: 60%;
        }
        @media (max-width: 1200px) {
            .db-subsystems {
                max-width: 50%;
            }
        }
        @media (max-width: 959px) {
            .db-subsystems {
                max-width: 100%;
            }
        }

        .thumbnail-container {
            max-height: 4rem;
            overflow: hidden;

            z-index: -1;
            opacity: .7;
            transition: all 0.5s ease-out;
            position: absolute;
        }

        td:hover .thumbnail-container {
            max-height: 17rem;

            opacity: 1;
            z-index: 10;
            transition: all 0.5s ease-out;
        }

        .thumbnail-container-container {
            height: 4.5rem;
            width: 7rem;
        }

        .thumbnail-container img {
            height: 4rem;
            width: 7rem;
            min-width: 0;

            object-fit: cover;
            object-position: center 2%;

            transition: 0.5s all;
        }

        td:hover .thumbnail-container img {
            height: 17rem;
            width: 17rem;
            min-width: 100%;
        }

        @media screen and (max-width: 959px) {
            .db-creator {
               display: inline-block;
            }

            .db-creator:not(:last-child)::after {
                content: ',';
                margin-right: 0.2em;
                display: inline;
            }

            .uk-table-responsive td:not(.uk-table-link) {
                padding-bottom: 3px !important;
            }
            .uk-table-responsive td:not(:first-child):not(.uk-table-link) {
                padding-top: 3px !important;
            }

            .uk-navbar {
                display: block;
                text-align: center;
            }

            .uk-navbar > * {
                justify-content: space-evenly;
                flex-wrap: wrap;

                width: 100%;

                position: initial !important;
                transform: none !important;
            }

            .uk-navbar-right .uk-navbar-item {
                padding: 0px 7px !important;
                flex: 1 1 auto;
                min-height: 40px;
            }

            .uk-navbar-right .uk-button {
                padding: 0 15px !important;
                line-height: 28px !important;
                font-size: 14px !important;
                width: 100%;
            }

            .db-subsystem {
                min-width: 75px;
            }
        }

        @media screen and (min-width: 960px) {
            .db-creator {
                max-width: 10em;
                margin-top: 0 !important; /* No spacing between list elements */

                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }
        }

        .db-subsystem {
            max-width: 150px;
        }

        header {
            background-color: #fefefe;
            border-bottom: 1px solid #e5e5e5;
            box-shadow: 5px 0 5px rgba(0,0,0,0.1);
        }

        .uk-input {
            transition-property: all;
        }
        .uk-input:focus {
            box-shadow: 0 0 5px #1e87f02b;
        }

        .uk-description-list > dt {
            font-weight: 500;
        }

        .db-small-padding-sides {
            padding: 0 15px;
        }

        .slbContentOuter {
            padding: 1em 1em !important;
        }

        .slbCloseBtn {
            top: -1em !important;
        }

        .db-iframe-container {
            background: url('<?php echo base_dir(); ?>/assets/loading.svg') center center no-repeat;
        }

        .db-category-description {
            max-width: 25em;
            text-align: justify;
        }
    </style>
</head>
<body>
<!--HEADER-->
<?php
    if (!isset($subsystem)) $subsystem = null;
    if (!isset($page)) $page = null;
?>
<header data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top; media: @m">
    <div class="uk-container-expand uk-padding-small">
        <nav id="navbar" uk-navbar>

            <div class="uk-navbar-left">
                <div class="uk-navbar-item db-subsystems">
                    <div class="uk-flex uk-flex-wrap" style="justify-content: space-evenly">
                        <?php foreach ($docid->getSubsystems() as $id) {
                            ?>
                            <a href="<?php
                                echo htmlentities($container->get(Router::class)->pathFor('list', [
                                    'filt1' => $id
                                ]));
                            ?>" class="uk-button
                                <?php echo $subsystem === $id ? 'uk-button-primary' : 'uk-button-default' ?>
                                uk-button-small
                            uk-margin-small-top uk-margin-small-right uk-flex-1 db-subsystem"
                               title="<?php echo $docid->getSubsystem($id); ?>"
                               ><?php
                                echo $id;
                            ?></a>
                        <?php
                        }
                        ?>
                    </div>

                </div>
            </div>

            <div class="uk-navbar-center">
                <a class="uk-navbar-item uk-logo" href="<?php
                echo htmlentities($container->get(Router::class)->pathFor('list'));
                ?>">AcubeSAT</a>
            </div>
            <div class="uk-navbar-right">
                <div class="uk-navbar-item">
                    <a title="Invalid entries" href="<?php
                    echo htmlentities($container->get(Router::class)->pathFor('list', [], [
                        'invalid' => true
                    ]));
                    ?>" class="uk-button <?php echo $page === 'invalid' ? 'uk-button-danger' : 'uk-button-default' ?>">Invalid</a>
                </div>

                <div class="uk-navbar-item">
                    <a href="<?php
                    echo htmlentities($container->get(Router::class)->pathFor('list'));
                    ?>" class="uk-button <?php echo $subsystem === null && $page === 'list' ? 'uk-button-primary' :
                    'uk-button-default' ?>">Documentation</a>
                </div>

                <div class="uk-navbar-item">
                    <a href="<?php
                    echo htmlentities($container->get(Router::class)->pathFor('categories'));
                    ?>" class="uk-button <?php echo $page === 'categories' ? 'uk-button-primary' : 'uk-button-default' ?>">Categories</a>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--/HEADER-->
