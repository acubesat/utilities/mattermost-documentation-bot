<?php

// Helper functions for PHP templates

use Psr\Http\Message\ServerRequestInterface;
use Slim\Route;
use Slim\Router;

/**
 * Return a path to this route
 *
 * @param array $data The additional data to add
 * @param array $queryParams The additional query parameters to add
 * @return string The edited path
 */
function path_to_here(array $data = [], array $queryParams = []) {
    global $container;

    return $container->get(Router::class)->pathFor($container->get(Route::class)->getName(),
        $data + $container->get(Route::class)->getArguments(),
        $queryParams + $container->get(ServerRequestInterface::class)->getQueryParams()
    );
}

/**
 * Returns the base directory without the currently browsed file
 * @return string
 */
function base_dir() {
    global $container;

    return $container->get('assetPath');
}

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

     $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}
