<?php


use Slim\Router;

require __DIR__ . '/header.php';
?>

<section class="uk-section uk-article">
    <div class="uk-container uk-container-small">
        <form class="uk-inline" style="float:right" action="#">
            <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: search"></span>
            <input class="uk-input uk-form-large uk-form-width-medium" id="js--input-search" autofocus>
        </form>
        <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top"><!--
            -->AcubeSAT Documentation List<!--
            --></h2>
        <?php if(isset($subsystem)) { ?>
            <div class="uk-text-meta"><?php echo $docid->getSubsystem($subsystem) ?></div>
        <?php } ?>
        <?php if(isset($category)) { ?>
            <div class="uk-text-meta"><?php echo $docid->formatCategories(str_split($category)) ?></div>
        <?php } ?>
    </div>

    <!-- large image -->
    <div class="uk-container uk-section">
        <?php if (empty($entries)) { ?>
            <div class="uk-alert">No documentation entries found here.</div>
        <?php } else { ?>

        <table class="uk-table uk-table-divider uk-table-responsive uk-text-center uk-text-left@m" id="js--list">
            <thead>
            <tr>
                <th><a class="<?php echo $sort === 'docid' ? 'uk-text-bold' : ''; ?>" href="<?php
                    echo htmlentities(path_to_here([], ['sort' => 'docid']));
                ?>">ID</a></th>
                <th><a class="<?php echo $sort === 'subsystem' ? 'uk-text-bold' : ''; ?>"
                       href="<?php
                    echo htmlentities(path_to_here([], ['sort' => 'subsystem']));
                    ?>">Subsystem</a></th>
                <th><a class="<?php echo $sort === 'title' ? 'uk-text-bold' : ''; ?>" href="<?php
                    echo htmlentities(path_to_here([], ['sort' => 'title']));
                    ?>">Title</a></th>
                <th>Download</th>
                <th class="uk-visible@l">Thumbnail</th>
                <th><a class="<?php echo $sort === 'author' ? 'uk-text-bold' : ''; ?>" href="<?php
                    echo htmlentities(path_to_here([], ['sort' => 'author']));
                    ?>">Author</a></th>
                <th><a class="<?php echo $sort === 'created' ? 'uk-text-bold' : ''; ?>" href="<?php
                    echo htmlentities(path_to_here([], ['sort' => 'created']));
                    ?>">Date</a></th>
            </tr>
            </thead>
            <tbody>
            <?php /** @var array $entries */
            $i = 0; // A counter needed for javascript
            foreach ($entries as $entry) {
                $i++;
                /** @var string $url The escaped URL of the 'show' action for this entry */
                if (isset($entry['docid'])) {
                    $url = htmlentities($container->get(Router::class)->pathFor('show', ['docid' => $entry['docid']]));
                } else {
                    $url = '#';
                }

                // A small data package that will be included with the HTML
                $datapack = htmlentities(json_encode([
                    'title' => $entry['title'] ?? '',
                    'docid' => $entry['docid'] ?? '',
                    'author' => implode("\n", $entry['authors'] ?? []),
                ]));

                echo "\n<tr class='js--entry' id='js--entry-$i' data-data=\"$datapack\">\n";

                // The documentation ID
                echo "<td>";
                echo "<a href='$url'><code class='js--entry-docid'>";
                if ($sort === 'docid') {
                    echo preg_replace('/-([^-]*-[^-]*)$/', '-<strong>$1</strong>', $entry['docid'] ?? '');
                } else {
                    echo $entry['docid'] ?? '';
                }
                echo "</code></a>";
                echo "</td>";

                // The subsystem
                if (isset($subsystem)) {
                    echo "<td class='uk-text-meta'>" . $entry['subsystem'] . "</td>";
                } else {
                    echo "<td>" . $docid->getSubsystem($entry['subsystem'] ?? null, true) . "</td>";
                }

                // The title
                echo "<td><a href='$url' class='uk-link-text'><strong class='js--entry-title'>" .
                    htmlentities($entry['title'] ?? $entry['filename']) . "</strong></a><br>";
                echo "<span class='uk-text-small uk-text-meta'>" .
                    $docid->formatCategories($entry['categories'] ?? []) . "</span>";
                if ($page === 'invalid') {
                    if (isset($entry['error'])) { ?>
                        <br>
                        <span class="uk-text-danger">Failure
                            reason:</span>
                        <?php echo $entry['error']; ?>
                        <?php
                    }
                }

                echo "</td>";


                echo "<td><div class='uk-button-group uk-width-1-1@s'>";
                if (isset($entry['links']['embedded'])) {
                    echo "<a class='uk-button uk-button-primary db-small-padding-sides uk-flex-1' target='_blank' title='View (in browser)' href='" .
                        htmlentities(\App\Operations\Registry::getEmbeddedUrl($entry)) . "'><i class='fas fa-binoculars'></i></a>";
                }
                if (isset($entry['links']['view'])) {
                    echo "<a class='uk-button uk-button-primary db-small-padding-sides uk-flex-1' target='_blank' title='View (on Google Drive)' href='" .
                        htmlentities($entry['links']['view'])
                        . "'><i class='fab fa-google-drive'></i></a>";
                }
                if (isset($entry['links']['download'])) {
                    echo "<a class='uk-button uk-button-secondary uk-flex-1' target='_blank' title='Download' href='" . htmlentities
                        ($entry['links']['download']) .
                        "'><i class='fas fa-download'></i></a>";
                }
                echo "</div></td>";

                // The thumbnail
                echo "<td class='uk-visible@l'>";
                if (isset($entry['thumbnail'])) {
                    ?>
                    <div class="thumbnail-container-container">
                    <div class="uk-text-center thumbnail-container uk-box-shadow-hover-large" style="">
                        <img
                             data-src="<?php echo htmlentities($container->get('assetPath') .
                                 $entry['thumbnail']); ?>"
                             alt="" uk-img>
                    </div>
                    </div>
                    <?php
                }
                echo "</td>";

                // The author
                echo "<td class='js--entry-author'>";
                if (isset($entry['authors']) && !empty($entry['authors'])) {
                    echo "<ul class='uk-list uk-text-small'>";
                    foreach ($entry['authors'] as $author) {
                        echo "<li class='db-creator'>" . htmlentities($author) . "</li>";
                    }
                    echo "</ul>";
                }
                echo "</td>";

                // The creation date
                echo "<td title='". strftime("%e %B %Y %H:%M", strtotime($entry['created'])) ."'>" .
                    (($sort === 'created') ?
                        ('<b>' . strftime("%e %B %Y", strtotime($entry['created'])) . '</b>') :
                        (strftime("%d/%m/%Y", strtotime($entry['created'])))) .
                    "</td>";

                // A quick preview link
                echo "<td class='uk-visible@m'>";
                if ($entry && $link = \App\Operations\Registry::getPdfjsUrl($entry, $container->get(Router::class), $container->get('assetPath'))) {
                    ?>
                    <a class="js--quick-preview uk-text-muted" title="Quick preview" aria-label="Quick preview"
                       href="<?php echo htmlentities($link); ?>">
                        <i class="fas fa-eye"></i>
                    </a>

                    <?php
                }
                echo "</td>";
                echo "</tr> ";
            }


            ?>
            </tbody>
        </table>

        <?php } ?>
    </div>
</section>

<?php
require 'footer.php';
?>
