<?php

use App\Operations\DocID;
use App\Operations\Exceptions\UserParseException;
use App\Operations\Registry;
use App\Parser\PDFParser;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Request;
use Slim\Route;
use Slim\Router;

require __DIR__ . '/header.php';
?>

<section class="uk-section uk-article">
    <div class="uk-container uk-container-small">
        <form class="uk-inline" style="float:right" action="#">
            <a aria-label="Jump to document" class="uk-button uk-button-primary " href="#db-document"
               title="Jump to document"><i class="fas fa-angle-double-down"></i></a>
        </form>
        <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top"><!--
            --><?php echo htmlentities($entry['title']); ?><!--
            --></h2>
        <div class="uk-text-meta"><?php echo htmlentities($entry['docid']); ?></div>
    </div>

    <div class="uk-container uk-section">
        <div class="uk-flex uk-grid-divider uk-margin-bottom">
            <div class="uk-first-column uk-flex-auto">
                <dl class="uk-description-list">
                    <dt>Title</dt>
                    <dd><?php echo htmlentities($entry['title']); ?></dd>

                    <dt>Filename</dt>
                    <dd>
                        <pre><?php echo htmlentities($entry['filename']); ?></pre>
                    </dd>

                    <dt>Upload Date</dt>
                    <dd title="<?php echo htmlentities($entry['created']); ?>"><?php echo htmlentities((new
                        DateTime($entry['created']))->format('d/m/Y')); ?></dd>

                    <dt>File Size</dt>
                    <dd title="<?php echo $entry['size']; ?> bytes"><?php echo formatBytes($entry['size'], 0); ?></dd>
                </dl>
            </div>
            <div class="uk-flex-auto">
                <dl class="uk-description-list">
                    <dt>Authors</dt>
                    <dd>
                        <?php if (isset($entry['authors'])) { ?>
                            <ul class="uk-list">
                                <?php foreach ($entry['authors'] as $author) { ?>
                                    <li class="uk-margin-remove"><?php echo htmlentities($author); ?></li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <em>?</em>
                        <?php } ?>
                    </dd>

                    <dt>Pages</dt>
                    <dd><?php echo $entry['pages'] ?? '<em>?</em>' ?></dd>

                    <dt>Modification Date</dt>
                    <dd title="<?php echo htmlentities($entry['modified']); ?>"><?php echo htmlentities((new DateTime
                        ($entry['modified']))->format
                        ('d/m/Y')); ?></dd>

                    <dt><span title="Documentation">Doc.</span> Template Version</dt>
                    <dd><?php echo $entry['templateVersion'] ?? '<em>?</em>' ?></dd>
            </div>
            <div class="uk-flex-auto">
                <dl class="uk-description-list">
                    <dt>Subsystem</dt>
                    <dd><?php echo $docid->getSubsystem($entry['subsystem']) ?></dd>

                    <dt>Categories</dt>
                    <dd>
                        <ul class="uk-breadcrumb">
                            <?php foreach ($docid->getCategories($entry['categories']) as $catId => $category) { ?>
                                <li><a href="<?php
                                    echo htmlentities($container->get(Router::class)->pathFor('list', [
                                        'filt1' => $catId
                                    ]));
                                    ?>"><?php echo $category; ?></a></li>
                            <?php } ?>
                        </ul>
                    </dd>

                    <dt>Version</dt>
                    <dd><?php echo $entry['version'] ?? '<em>?</em>' ?></dd>

                    <dt>Public?</dt>
                    <dd><?php echo ($entry['public'] ?? null) ? 'Yes' : 'No' ?></dd>

                    <dt>Confidential?</dt>
                    <dd><?php echo ($entry['confidential'] ?? null) ? '<span class="uk-text-danger">Yes</span>' : 'No' ?></dd>
                </dl>
            </div>
        </div>
        <?php if (!empty($entry['links'] ?? [])) { ?>
            <div>
                <div class="uk-button-group uk-width-1-1 uk-margin-bottom">
                    <?php if(isset($entry['links']['embedded'])) { ?>
                    <a class='uk-button uk-width-1-2 uk-button-primary uk-button-large' target='_blank' title='View'
                       href='<?php echo htmlentities(Registry::getEmbeddedUrl($entry, $page = true)); ?>'>
                        <i class='fas fa-binoculars'></i> View file in browser
                    </a>
                    <?php }
                    if(isset($entry['links']['view'])) { ?>
                    <a class='uk-button uk-width-1-2 uk-button-danger uk-button-large' target='_blank' title='View'
                       href='<?php echo htmlentities($entry['links']['view']); ?>'>
                        <i class='fab fa-google-drive'></i> View file on Google Drive
                    </a>
                    <?php }
                    if(isset($entry['links']['download'])) { ?>
                    <a class='uk-button uk-width-1-2 uk-button-secondary uk-button-large' target='_blank'
                       title='Download'
                       href='<?php echo htmlentities($entry['links']['download']); ?>'>
                        <i class='fas fa-download'></i> Download file
                    </a>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <?php if (!empty($entry['versions'] ?? [])) { ?>
        <h4>Document Versions</h4>
        <table class="uk-table uk-table-divider">
            <thead>
            <tr>
                <th>Version</th>
                <th>Date</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($entry['versions'] as $version) { ?>
                <tr>
                    <td><?php echo htmlentities($version['version']); ?></td>
                    <td><?php echo htmlentities($version['date']); ?></td>
                    <td><?php echo htmlentities($version['status']); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
            <?php
        } ?>

    </div>
</section>

<a id="db-document"></a>
<section class="uk-container uk-container-expand">
        <?php if(isset($entry['docid'])) { ?>
            <iframe src="<?php
                echo htmlentities(Registry::getPdfjsUrl($entry, $container->get(Router::class), $container->get('assetPath')));
            ?>" frameborder="0" style="height:90vh; width:100%">
            </iframe>
        <?php } ?>
</section>

<?php
require 'footer.php';
?>
