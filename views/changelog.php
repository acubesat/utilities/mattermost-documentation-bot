<?php

use App\Operations\Configuration;

require __DIR__ . '/header.php';

/** @var Gitlab\Model\Tag[] $tags */
/** @var Gitlab\Model\Commit[][] $commits */

global $container;

?>

<section class="uk-section uk-article">
    <div class="uk-container uk-container-small">
        <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">AcubeSAT
            Documentation Template Changelog
        </h2>
    </div>

    <!-- large image -->
    <?php foreach($tags as $i => $tag) { ?>
        <div class="uk-container uk-section">
            <div class="uk-card <?php echo ($i < 0) ? '' : 'uk-card-default' ?> uk-card-hover uk-card-large uk-card-body">
                <?php
                    if ($i >= 0) {
                ?>
                        <h3 class="uk-card-title">Version <?php echo $tag->name;  ?></h3>
                <?php } ?>
                <p>
                <table class="uk-table">
                    <caption></caption>
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (($commits[$i] ?? []) as $commit) { ?>
                        <tr>
                            <td class="uk-text-muted"><?php echo $commit->short_id ?></td>
                            <?php
                                $url ="https://gitlab.com/" . $container->get(Configuration::class)->gitlab->template_repo . "/commits/" . $commit->id;
                            ?>
                            <td><a class="uk-link-text" target="_blank" rel="noopener" href="<?php echo $url ?>">
                                    <?php echo $commit->title ?>
                                </a>
                            </td>
                            <td>
                                <?php echo strftime('%e %B %Y', strtotime($commit->authored_date)); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                </p>
            </div>
        </div>
    <?php } ?>
    <div class="uk-container uk-section">

    </div>

</section>

<?php
require 'footer.php';
?>
